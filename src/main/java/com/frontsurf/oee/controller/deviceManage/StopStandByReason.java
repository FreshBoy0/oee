package com.frontsurf.oee.controller.deviceManage;


import com.frontsurf.oee.common.group.Common;
import com.frontsurf.oee.model.OeeDeviceLoadScheme;
import com.frontsurf.oee.model.OeeStopStandbyReasons;
import com.frontsurf.oee.service.deviceManage.StopStandByCauseService;
import com.frontsurf.oee.util.Return;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/device")
public class StopStandByReason {

    @Autowired
    StopStandByCauseService stopStandByCauseService;

    /**
     * 录入设备负荷计划信息
     *
     * @param oeeStopStandbyReasons
     * @return
     */
    @RequestMapping(value = "/StopStandByReason", method = RequestMethod.POST)
    public Return addStopStandByReason(@RequestBody OeeStopStandbyReasons oeeStopStandbyReasons)
    {
        return stopStandByCauseService.insertStopStandByCause(oeeStopStandbyReasons);
    }



}
