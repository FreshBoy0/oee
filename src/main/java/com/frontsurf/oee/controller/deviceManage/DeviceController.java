package com.frontsurf.oee.controller.deviceManage;

import com.frontsurf.oee.common.group.Common;
import com.frontsurf.oee.common.group.Update;
import com.frontsurf.oee.mapper.deviceManage.OeeDeviceMapper;
import com.frontsurf.oee.model.OeeDevice;
import com.frontsurf.oee.service.deviceManage.DeviceService;
import com.frontsurf.oee.util.Return;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/device")
public class DeviceController {
    @Autowired
    DeviceService deviceService;

    @Autowired
    OeeDeviceMapper oeeDeviceMapper;
    /**
     * 录入设备信息
     *
     */
    @RequestMapping(value = "/addDevice", method = RequestMethod.POST)
    public Return addDevice(@RequestBody @Validated(Common.class) OeeDevice oeeDevice) {
        return deviceService.insertDevice(oeeDevice);
    }
    /**
     * 根据id更新设备信息
     *      *
     * @param oeeDevice
     * @return
     */
    @RequestMapping(value = "/updateDeviceById", method = RequestMethod.POST)
    public Return updateDeviceById(@RequestBody  OeeDevice oeeDevice) {
        return deviceService.updateDeviceById(oeeDevice);
    }

//    /**
//     * 根据设备Id删除设备信息
//     *
//     * @param id
//     * @return
//     */
//    @RequestMapping(value = "/deleteDeviceById", method = RequestMethod.POST)
//        public Return deleteDeviceById(@RequestParam("id") int id) {
//        return deviceService.deleteDeviceById(id);
//    }
    /**
     * 根据设备编号查询设备信息
     *
     */
    @RequestMapping(value = "/queryDeviceBySerialNum", method = RequestMethod.GET)
    public Return queryDeviceBySerialNum(@RequestParam("SerialNum") String SerialNum) {
        return deviceService.queryDeviceBySerialNum(SerialNum);
    }
//    /**
//     * 根据设备Id查询设备信息
//     *
//      */
//    @RequestMapping(value = "/queryDeviceById", method = RequestMethod.GET)
//    public Return deleteDeviceById(@RequestParam("id") int id) {
//        return deviceService.queryDeviceById(id);
//    }


    /**
     * 根据设备名查询设备信息
     *
     * @param name 设备名称
     * @return
     */
    @RequestMapping(value = "/queryDeviceByName", method = RequestMethod.GET)
    public Return queryDeviceByName(@RequestParam("name") String name) {

        return deviceService.queryDeviceByName(name);
    }

    /**
     * 查询所有设备信息
     *
     * @return
     */
    @RequestMapping(value = "/queryAllDevice", method = RequestMethod.GET)
    public Return queryAllDevice() {
        return deviceService.queryAllDevice();
    }


}

























