package com.frontsurf.oee.controller.deviceManage;


import com.frontsurf.oee.common.group.Common;
import com.frontsurf.oee.common.group.Update;
import com.frontsurf.oee.model.OeeDeviceLoadScheme;
import com.frontsurf.oee.service.deviceManage.deviceLoadSchemeService;
import com.frontsurf.oee.util.Return;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/device/loadScheme")
public class DeviceLoadScheme {


    @Autowired
    deviceLoadSchemeService deviceLoadSchemeService;
    /**
     * 录入设备负荷计划信息
     *
     * @param oeeDeviceLoadScheme
     * @return
     */
    @RequestMapping(value = "/addDeviceLoadScheme", method = RequestMethod.POST)
    public Return addDevice(@RequestBody @Validated(Common.class) OeeDeviceLoadScheme oeeDeviceLoadScheme)
    {
        return deviceLoadSchemeService.insertDeviceLoadScheme(oeeDeviceLoadScheme);
    }

    /**
     * 更新设备负荷计划信息
     *
     * @param oeeDeviceLoadScheme
     * @return
     */
    @RequestMapping(value = "/updateDeviceLoadScheme", method = RequestMethod.POST)
    public Return updateDeviceLoadSchemeById(@RequestBody @Validated({Update.class,Common.class}) OeeDeviceLoadScheme oeeDeviceLoadScheme) {
        return deviceLoadSchemeService.updateDeviceLoadSchemeById(oeeDeviceLoadScheme);
    }

    /**
     * 根据Id删除设备负荷计划信息
     *
     * @param id
     * @return
     */
    @RequestMapping(value = "/deleteDeviceLoadScheme", method = RequestMethod.POST)
    public Return deleteDeviceLoadSchemeById(@RequestParam("id") int id) {
        return deviceLoadSchemeService.deleteDeviceLoadSchemeById(id);
    }

    /**
     * 查询设备负荷计划信息
     *
     * @param name 计划名称
     * @return
     */
    @RequestMapping(value = "/queryDeviceLoadSchemeByName", method = RequestMethod.GET)
    public Return queryDeviceAttendanceScheme(@RequestParam String name) {
        return deviceLoadSchemeService.queryDeviceLoadSchemeByName(name);
    }

    /**
     * 查询所有的设备负荷计划信息
     *
     * @return
     */
    @RequestMapping(value = "/queryAllDeviceLoadScheme", method = RequestMethod.GET)
    public Return queryAllDeviceAttendanceScheme() {
        return deviceLoadSchemeService.queryAllDeviceLoadScheme();
    }



}




































