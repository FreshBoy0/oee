package com.frontsurf.oee.controller.deviceManage;


import com.frontsurf.oee.common.group.Common;
import com.frontsurf.oee.mapper.deviceManage.BrandsTypes;
import com.frontsurf.oee.mapper.deviceManage.OeeDeviceBrandMapper;
import com.frontsurf.oee.mapper.deviceManage.OeeDeviceTypeMapper;
import com.frontsurf.oee.model.OeeBrandtypes;
import com.frontsurf.oee.service.deviceManage.BrandTypeService;
import com.frontsurf.oee.util.Return;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/device/brandType")
public class BrandTypeController {

    @Autowired
    BrandTypeService brandTypeService;


    /**
     * 录入品牌型号信息
     *
     */
    @RequestMapping(value = "/addBrandTypes", method = RequestMethod.POST)
    public Return insertBrand(@RequestBody  OeeBrandtypes oeeBrandtypes)
    {
            return brandTypeService.insertBrand(oeeBrandtypes) ;
    }

    /**
     * 查看品牌及其对应型号信息
     *
     */
    @RequestMapping(value = "/selectBrandTypes", method = RequestMethod.GET)
    public Return queryTypeNameByBrandName(@RequestParam String brandName)
    {
        return brandTypeService.queryTypeNameByBrandName(brandName) ;
    }



}
