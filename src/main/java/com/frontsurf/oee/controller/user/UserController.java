package com.frontsurf.oee.controller.user;

import com.frontsurf.oee.model.User;
import com.frontsurf.oee.service.user.UserService;
import com.frontsurf.oee.util.Return;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author liu.zhiyang 2019/5/16 13:31
 * @Email liu.zhiyang@frontsurf.com
 */
@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    UserService userService;

    @RequestMapping(value = "/list",method = RequestMethod.GET)
    public Return userList() {
        return userService.userList();
    }

    @RequestMapping("/getCurrentUserInformation")
    public Return getCurrentUser()
    {
        Object user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return Return.success(user,"获取当前用户信息成功");
    }

    @RequestMapping("/userRole")
    public Return userRole() {
        return userService.userRole();
    }
}
