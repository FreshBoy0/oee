package com.frontsurf.oee.controller.login;



import com.frontsurf.oee.mapper.user.PermissionMapper;
import com.frontsurf.oee.model.Permission;
import com.frontsurf.oee.model.Role;
import com.frontsurf.oee.model.User;
import com.frontsurf.oee.util.Return;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * @Author xu.xiaojing
 * @Date 2019/3/29 16:34
 * @Email xu.xiaojing@frontsurf.com
 * @Description
 */

@RestController
@RequestMapping("/login")
public class LoginController {



    @Autowired
    PermissionMapper permissionMapper;

    /**
     * 登录成功
     *
     * @return
     */
    @RequestMapping("/success")
    public Return loginSucess(HttpServletResponse response) {
        //解決代理的跨域问题
        response.setHeader("Access-Control-Allow-Origin", "*");
        return Return.success("登录成功");
    }

    /**
     * 登录失败
     *
     * @return
     */
    @RequestMapping("/fail")
    public Return loginFail(HttpServletResponse response, String message) {

        //解決代理的跨域问题
        response.setHeader("Access-Control-Allow-Origin", "*");
        return Return.fail(message);
    }

    /**
     * 访问拒绝，没有相应的权限
     *
     * @return
     */
    @RequestMapping("/access/denied")
    public Return accessDenied(HttpServletResponse response) {
//解決代理的跨域问题
        response.setHeader("Access-Control-Allow-Origin", "*");
        return Return.fail(Return.UNAUTHORIZED_ERROR, "访问失败，没有此权限");
    }

    /**
     * 访问拒绝，没有进行登录，无法进行身份验证
     *
     * @return
     */
    @RequestMapping("/access/unauthenticated")
    public Return unauthenticated(HttpServletResponse response) {
        //解決代理的跨域问题
        response.setHeader("Access-Control-Allow-Origin", "*");
        return Return.fail(Return.UNAUTHENTICATED_ERROR, "访问失败，尚未登录，请先登录");
    }

    /**
     * 登出成功
     *
     * @param response
     * @return
     */
    @RequestMapping("/logout/success")
    public Return logout(HttpServletResponse response) {

        //解決代理的跨域问题
        response.setHeader("Access-Control-Allow-Origin", "*");
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (SecurityContextHolder.getContext().getAuthentication() != null && SecurityContextHolder.getContext().getAuthentication().isAuthenticated() && !SecurityContextHolder.getContext().getAuthentication().getPrincipal().equals("anonymousUser")) {
            return Return.fail(Return.COMMON_ERROR, "尚未登出，请重试");
        }
//        //释放资源锁
//        try {
//            ApplicationContext.getSchedulePlanLock().releaseLock();
//        } catch (DataException e) {
//            e.printStackTrace();
//        }
        return Return.success("登出成功");
    }

    /**
     * 获取当前用户信息
     *
     * @param response
     * @return
     */
    @RequestMapping("/user")
    public Return getCurrentUser(HttpServletResponse response) {

        SecurityContext securityContextHolder = SecurityContextHolder.getContext();
        if (SecurityContextHolder.getContext().getAuthentication().isAuthenticated() && SecurityContextHolder.getContext().getAuthentication().getPrincipal().equals("anonymousUser")) {
            return Return.fail(Return.UNAUTHENTICATED_ERROR, "尚未登录，请登录");
        }
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Map<String, Object> userInfoMap = new HashMap<>();
        userInfoMap.put("username", user.getName());
        userInfoMap.put("permissions", this.getUserPermissionCode(user));
        return Return.success(userInfoMap);
    }

    private Set<String> getUserPermissionCode(User user) {
        Set<String> codes = new HashSet<>();
        if (user.getRoles() != null) {
            for (Role role : user.getRoles()) {
                if (role.getPermissions() != null) {
                    for (Permission permission : role.getPermissions() ) {
                        codes.add(permission.getCode());
                        //处理子级权限
                        if (permission.getChildren() != null) {
                            for (Permission childPer : permission.getChildren()) {
                                codes.add(childPer.getCode());
                            }
                        }

                        //处理此权限的父级权限
                        while (permission.getParentId() != null) {
                            Permission parent = permissionMapper.selectById(permission.getParentId());
                            if (parent == null) {
                                break;
                            }
                            codes.add(parent.getCode());
                            permission = parent;
                        }
                    }
                }
            }
        }
//        //判断是否是租户管理员
//        if (mesUser.getMesId().equals(tenantAdminMesId)) {
//            codes.add(tenantAdminPerCode);
//        }
        return codes;
    }


}
