package com.frontsurf.oee.util;

import com.frontsurf.oee.model.User;
import org.springframework.security.core.context.SecurityContextHolder;

public class UserInfo {

    public static String getUserName() throws DataException {
        String name;
        try {
            User mesUser = getCurrentUser();
            name = mesUser.getUsername();
        } catch (Exception e) {
            name = null;
        }
        if (name == null) {
            throw new DataException(400, "用户未登录，获取不了用户名");
        }
        return name;
    }

    public static Integer getUserId() throws DataException {
        Integer id;
        try {
            User user = getCurrentUser();
            id = user.getId();
        } catch (Exception e) {
            id = null;
        }
        if (id == null) {
            throw new DataException(Return.COMMON_ERROR, "用户尚未登录，获取不了用户ID");
        }
        return id;
    }

    public static <T extends User> T getCurrentUser() throws DataException {
        T user = null;
        try {
            user = (T) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        } catch (Exception e) {
            user = null;
        }
        if (user == null) {
            throw new DataException(Return.COMMON_ERROR, "用户尚未登录，获取不了用户ID");
        }
        return user;
    }

}
