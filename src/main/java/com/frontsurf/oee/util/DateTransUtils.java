package com.frontsurf.oee.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAccessor;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 *日期转换工具类
 */
public class DateTransUtils {
    private static ThreadLocal<Map<String,SimpleDateFormat>> dateFormatMap=new ThreadLocal<Map<String, SimpleDateFormat>>(){
        @Override
        protected Map<String,SimpleDateFormat> initialValue(){
            return new HashMap<String,SimpleDateFormat>();
        }
    };

    private static SimpleDateFormat getSimpleDateFormat(final String pattern){
        Map<String,SimpleDateFormat> map=dateFormatMap.get();
        SimpleDateFormat simpleDateFormat=map.get(pattern);
        if(simpleDateFormat==null){
            simpleDateFormat=new SimpleDateFormat(pattern);
            map.put(pattern,simpleDateFormat);
        }
        return simpleDateFormat;
    }

    public static String format(Date date, String pattern){
        return getSimpleDateFormat(pattern).format(date);
    }

    public static Date parse(String date,String pattern) throws ParseException {
        return getSimpleDateFormat(pattern).parse(date);
    }

    public static String timeStampToDate(long timestamp){
        String date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date(timestamp));
        return date;
    }

    public static String timeStampToDate(String timestampString){
        Long timestamp = Long.parseLong(timestampString);
        return timeStampToDate(timestamp);
    }



    public static void main(String[] args) throws ParseException {
       // System.out.println( LocalDateTime.now().format( DateTimeFormatter.ofPattern("yyyMMdd")));
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
      //  LocalDateTime parse = LocalDateTime.parse("2019-05-06 00:00:00",dateTimeFormatter);
      //  LocalDateTime parse1 = LocalDateTime.parse("2019-05-12 00:00:00",dateTimeFormatter);
      //  System.out.println(parse.compareTo(parse1));
     //   SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        TemporalAccessor sDate = (TemporalAccessor) dateTimeFormatter.parse("2018-08-09");
        Calendar c = Calendar.getInstance();

        c.add(Calendar.DAY_OF_MONTH, 1);
        System.out.println("明天：" +      dateTimeFormatter.format((TemporalAccessor) c.getTime()));
    }

}
