package com.frontsurf.oee.util;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.*;

@Repository
public class PhoenixUtils {

    @Value("${phoenix.url}")
    private String phoenix_url;

    @Value("${phoenix.schema.isNamespaceMappingEnabled}")
    private String isNamespaceMappingEnabled;

    private Connection getConn() throws SQLException, ClassNotFoundException {
        //扫描一下包
        Class.forName("org.apache.phoenix.jdbc.PhoenixDriver");
        Properties props = new Properties();
        props.setProperty("phoenix.driverClassName", "org.apache.phoenix.jdbc.PhoenixDriver");
        props.setProperty("phoenix.schema.isNamespaceMappingEnabled", isNamespaceMappingEnabled);
        return DriverManager.getConnection(phoenix_url, props);
    }

    public void update(String sql) throws DataException {
        Connection conn = null;
        Statement stmt = null;
        try {
            conn = getConn();
            stmt = conn.createStatement();
            stmt.executeUpdate(sql);
            conn.commit();
        } catch (Exception e) {
            try {
                if (conn != null) conn.rollback();
            } catch (Exception rollbackError) {
                throw new DataException(400, "Phoenix回滚异常:"+ rollbackError.getMessage());
            }
            throw new DataException(400, "Phoenix插入SQL异常:" + e.getMessage());
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception closeError) {
                throw new DataException(400, "Phoenix关闭连接异常:" + closeError.getMessage());
            }
        }
    }

    public List<Map<String, Object>> query(String sql) throws DataException {
        List<Map<String, Object>> list = new ArrayList<>();
        try (
                Connection conn = getConn();
                Statement stmt = conn.createStatement();
                ResultSet rs = stmt.executeQuery(sql)
        ) {
            ResultSetMetaData md = rs.getMetaData();
            int columns = md.getColumnCount();
            while (rs.next()) {
                HashMap<String, Object> row = new HashMap<>();
                for (int i = 1; i <= columns; ++i) {
                    row.put(md.getColumnName(i), rs.getObject(i));
                }
                list.add(row);
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new DataException(400, "Phoenix查询SQL异常:" + e.getMessage());
        }
        return list;
    }

    public Map<String, Object> query(String sql, Integer pageNum, Integer pageSize) throws DataException {
        Map<String, Object> data = new HashMap<>();
        data.put("pageNum", pageNum);
        data.put("pageSize", pageSize);
        // 查询总数
        String sqlTotalFormat = "select count(1) as TOTAL from (%s)";
        String sqlTotal = String.format(sqlTotalFormat, sql);
        List<Map<String, Object>> totalMap = query(sqlTotal);
        Long total = 0L;
        if (totalMap.size()>0) total = Long.parseLong(query(sqlTotal).get(0).get("TOTAL").toString());
        data.put("total", total);

        // 查询翻页数据
        Integer offset = (pageNum - 1) * pageSize;
        String sqlWithPageFormat = " %s LIMIT %d OFFSET %d ";
        String sqlWithPage = String.format(sqlWithPageFormat, sql, pageSize, offset);
        List<Map<String, Object>> list = query(sqlWithPage);
        data.put("list", list);

        return data;
    }

    public <T> List<T> queryForList(String sql, Class<T> tClass) throws DataException {
        List<T> data = new ArrayList<T>();
        try (
                Connection conn = getConn();
                Statement statement = conn.createStatement();
                ResultSet rs = statement.executeQuery(sql);
        ) {
            data = resultSetToJsonArry(rs).toJavaList(tClass);

        } catch (Exception e) {
            e.printStackTrace();
            throw new DataException(400, "Phoenix SQL异常:" + e.getMessage());
        }
        return data;
    }

    private JSONArray resultSetToJsonArry(ResultSet rs) throws SQLException, JSONException {
        // json数组
        JSONArray array = new JSONArray();

        // 获取列数
        ResultSetMetaData metaData = rs.getMetaData();
        int columnCount = metaData.getColumnCount();

        // 遍历ResultSet中的每条数据
        while (rs.next()) {
            JSONObject jsonObj = new JSONObject();
            // 遍历每一列
            for (int i = 1; i <= columnCount; i++) {
                String columnName = metaData.getColumnLabel(i);
                String value = rs.getString(columnName);
                jsonObj.put(columnName, value);
            }
            array.add(jsonObj);
        }
        return array;
    }


}
