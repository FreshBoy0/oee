package com.frontsurf.oee.util;


import org.apache.commons.lang3.StringUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author liu.zhiyang 2018/12/6 18:03
 * @Email liu.zhiyang@frontsurf.com
 * <p>
 * SimpleDateFormat ：yyyy-MM-dd HH:mm:ss、yyyy-MM-dd、HH:mm:ss
 */
public class DateUtils {


    /**
     * 将时间转换为时间戳
     *
     * @param s
     * @return
     * @throws ParseException
     */
    public static String dateToStamp(String s) throws ParseException {
        String res;
        long ts = dateToStampLong(s);
        res = String.valueOf(ts);
        return res;
    }

    public static Long dateToStampLong(String s) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = null;
        try {
            date = simpleDateFormat.parse(s);
            return date.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return 0L;
    }


    /**
     * 时间字符串 转成 时间戳
     * 01:01  ---> 24*60*60*1000 + 1*60*1000
     *
     * @param s
     * @param pattern
     * @return
     */
    public static Long timeToStampLong(String s, String pattern) {
        if (pattern == null || "".equals(pattern)) {
            pattern = "HH:mm:ss";
        }
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        Date date = null;
        try {
            // 因为simpleDateFormat的日期使用默认：1970.1.1
            date = simpleDateFormat.parse(s);
            Date start = simpleDateFormat.parse("00:00");
            return date.getTime() - start.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return 0L;
    }

    /**
     * 时间戳 转换成 时间字符串 ,与timeToStampLong对应起来
     * 24*60*60*1000 + 1*60*1000 ---->  01:01
     *
     * @param stamp
     * @param pattern
     * @return
     */
    public static String timeStampToTimeString(long stamp, String pattern) {
        if (pattern == null || "".equals(pattern)) {
            pattern = "HH:mm:ss";
        }
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        try {
            Date start = simpleDateFormat.parse("00:00");
            Date date = new Date(stamp + start.getTime());
            return simpleDateFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }


    /**
     * 将时间戳转换为时间
     */
    public static String stampToDate(String s, String pattern) {
        if (pattern == null) {
            pattern = "yyyy-MM-dd HH:mm:ss";
        }
        String res;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        long lt = new Long(s);
        Date date = new Date(lt);
        res = simpleDateFormat.format(date);
        return res;
    }


    public static Calendar getCalendar(String timeZone) {
        String tz = StringUtils.isEmpty(timeZone) ? "GMT+8" : timeZone;
        TimeZone curTimeZone = TimeZone.getTimeZone("GMT+8");
        return Calendar.getInstance(curTimeZone);
    }

    /**
     * 获取当天（按当前传入的时区）00:00:00所对应时刻的long型值
     */
    public static long getStartTimeOfDay(String timeZone) {
        Calendar calendar = getCalendar(timeZone);
        calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH),
                0, 0, 0);
        return calendar.getTimeInMillis();
    }

    /**
     * 获取某天23:59:59所对应时刻的long值
     * n只距离当天相差多少天，如昨天23:59:59，设置n=1
     *
     * @param timeZone
     * @return
     */
    public static long getEndTimeOfDay(String timeZone, int n) {
        Calendar calendar = getCalendar(timeZone);
        calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH) - n,
                23, 59, 59);
        return calendar.getTimeInMillis();
    }


    public static String date2String(Date date, DateType dateType) {

        SimpleDateFormat sdf = null;
        switch (dateType) {
            case YEAR:
                sdf = new SimpleDateFormat("yyyy");
                break;
            case MONTH:
                sdf = new SimpleDateFormat("yyyy-MM");
                break;
            case DATE:
                sdf = new SimpleDateFormat("yyyy-MM-dd");
                break;
            case HOUR:
                sdf = new SimpleDateFormat("yyyy-MM-dd HH");
                break;
            case MINUTES:
                sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
                break;
            default:
                sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                break;
        }
        return sdf.format(date);
    }

    public static boolean dateFormateValite(String timeStr, String forMate) {
        switch (forMate) {
            case "yyyy-MM-dd HH:mm:ss":
                String regex = "^(?:(?!0000)[0-9]{4}-(?:(?:0[1-9]|1[0-2])-(?:0[1-9]|1[0-9]|2[0-8])|(?:0[13-9]|1[0-2])-(?:29|30)|(?:0[13578]|1[02])-31)|(?:[0-9]{2}(?:0[48]|[2468][048]|[13579][26])|(?:0[48]|[2468][048]|[13579][26])00)-02-29)\\s+([01][0-9]|2[0-3]):[0-5][0-9]:[0-5][0-9]$";
                Pattern p = Pattern.compile(regex);
                Matcher m = p.matcher(timeStr);
                return m.matches();
            case "yyyy-MM-dd":
                regex = "^(?:(?!0000)[0-9]{4}([-/.]?)(?:(?:0?[1-9]|1[0-2])([-/.]?)(?:0?[1-9]|1[0-9]|2[0-8])|(?:0?[13-9]|1[0-2])([-/.]?)(?:29|30)|(?:0?[13578]|1[02])([-/.]?)31)|(?:[0-9]{2}(?:0[48]|[2468][048]|[13579][26])|(?:0[48]|[2468][048]|[13579][26])00)([-/.]?)0?2([-/.]?)29)$";
                p = Pattern.compile(regex);
                m = p.matcher(timeStr);
                return m.matches();
            default:
                regex = "^(?:(?!0000)[0-9]{4}-(?:(?:0[1-9]|1[0-2])-(?:0[1-9]|1[0-9]|2[0-8])|(?:0[13-9]|1[0-2])-(?:29|30)|(?:0[13578]|1[02])-31)|(?:[0-9]{2}(?:0[48]|[2468][048]|[13579][26])|(?:0[48]|[2468][048]|[13579][26])00)-02-29)\\s+([01][0-9]|2[0-3]):[0-5][0-9]:[0-5][0-9]$";
                p = Pattern.compile(regex);
                m = p.matcher(timeStr);
                return m.matches();
        }
    }

    public static boolean isValidDate(String str) {
        return isValidDate(str,"yyyy-MM-dd HH:mm:ss");
    }

    public static boolean isValidDate(String str,String pattern) {
        boolean convertSuccess=true;

        //2019-02-22 18:11:18xx 都可以转换为
        Pattern p = Pattern.compile("[a-zA-z]");
        if(p.matcher(str).find()) {
            return false;
        }

        SimpleDateFormat format = new SimpleDateFormat(pattern);
        try {
            format.setLenient(false);
            Date date= format.parse(str);
            System.out.println(date.toString());
        } catch (ParseException e) {
            convertSuccess=false;
        }
        return convertSuccess;
    }


    public static String getNowDate(){
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        return format.format(Calendar.getInstance().getTime());
    }

    public static String getLastDate(int month){
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal = Calendar.getInstance();//使用默认时区和语言环境获得一个日历。
        cal.add(Calendar.MONTH, month);//取当前日期的后一天
        return format.format(cal.getTime());
    }

    public static int getNowHours(){
        Date today = new Date();
        SimpleDateFormat format = new SimpleDateFormat("HH");
        return Integer.parseInt(format.format(today));
    }

    public static void main(String[] args) throws ParseException {

//        System.out.println(getNowHours());
//        System.out.println(System.currentTimeMillis());
//        System.out.println(getLastDate(-3));
//        System.out.println(getNowDate());
        System.out.println(DateUtils.stampToDate("1558082724736","yyyy-MM-dd HH:mm:ss"));

//        System.out.println(dateFormateValite("2019-04-09 10:23:23xxxx","yyyy-MM-dd HH:mm:ss"));

        //long xx = 1554977673941 - 1554977861514

//        String da = "2019-04-09 10:23:23";
//        System.out.println(dateToStamp(da));
//        System.out.println(DateUtils.getStartTimeOfDay("GMT+8"));
//        System.out.println(stampToDate(DateUtils.getStartTimeOfDay("GMT+8")+""));
//        Calendar calendar = Calendar.getInstance();
//        int hour = calendar.get(Calendar.HOUR_OF_DAY);
//        int min=calendar.get(Calendar.MINUTE);
//        System.out.println(hour+":"+min);


//        String keyStr = "PRODUCTCOUNT:00000001|19:9";
//        int length = keyStr.length();
//        if (length == 26) {
//            String substring = keyStr.substring(22);
//            int index = substring.lastIndexOf(":");
//            if (index == 1) {
//                substring = StringUtils.leftPad(substring, 5, "0");
//            } else if (index == 2) {
//                substring="";
//            }
//            keyStr =   substring;
//        } else if (length == 25) {
//            keyStr =   "0" + keyStr.substring(22) + "0";
//        }
//        System.out.println("2019-01-01 12:12:12".substring(0, 16));
//        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        System.out.println(dateFormat.format(dateTimeToDate(System.currentTimeMillis())));
    }

    public static Date dateTimeToDate(long mills) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(mills);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);

        return calendar.getTime();
    }


    public static int intervalDays(Date date1, Date date2) {
        long temp = 0;
        temp = date1.getTime() - date2.getTime();
        return (int) (temp / (24 * 60 * 60 * 1000));
    }

    /**
     * 获取日期所对应的星期几，
     *
     * @param date
     * @return
     */
    public static int dayOfWeek(Date date) {
        Calendar startCalendar = Calendar.getInstance();
        startCalendar.setTime(date);
        //Calendar是从星期天开始算起，星期天对应的是1
        int dayIndex = startCalendar.get(Calendar.DAY_OF_WEEK) - 1;
        dayIndex = dayIndex == 0 ? 7 : dayIndex;
        return dayIndex;
    }

    public enum DateType {
        YEAR, MONTH, DATE, HOUR, MINUTES, SECONDS;
    }

}
