package com.frontsurf.oee.util;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * @author liu.zhiyang 2019/6/6 10:07
 * @Email liu.zhiyang@frontsurf.com
 */
public class IpAddress {

    public static InetAddress netAddress=getInetAddress();

    public static InetAddress getInetAddress(){
        try{
            return InetAddress.getLocalHost();
        }catch(UnknownHostException e){
            System.out.println("unknown host!");
        }
        return null;

    }

    public static String getHostIp(){

        if(null == netAddress){
            return null;
        }
        //get the ip address
        return netAddress.getHostAddress();
    }

    public static String getHostName(){
        if(null == netAddress){
            return null;
        }
        //get the host address
        return netAddress.getHostName();
    }
}
