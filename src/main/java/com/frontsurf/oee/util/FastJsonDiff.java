//package com.frontsurf.deviceOee.util;
//
//import com.alibaba.fastjson.JSONObject;
//
//import java.util.ArrayList;
//import java.util.Arrays;
//import java.util.Iterator;
//import java.util.List;
//
///**
// * @author liu.zhiyang 2019/5/23 9:55
// * @Email liu.zhiyang@frontsurf.com
// */
//public class FastJsonDiff {
//
//
//    static final String[] ignoreStr = new String[]{"TMMACHINEIP", "TMCRAFTID", "TIMESTAMP", "TMUPDATENEW",
//            "TMUPDATEOLD", "TMUPDATECONTENT", "TMONLINESTATE", "TMALARMSTATE", "TMALARMID", "TMVENDOR"};
//
//    /**
//     * @param json1 new JSON
//     * @param json2 old JSON
//     * @return
//     */
//    @SuppressWarnings("unchecked")
//    public static List<FieldRelation> compareJson(JSONObject json1, JSONObject json2) {
//        Iterator<String> i1 = json1.keySet().iterator();
//        List<FieldRelation> frList = new ArrayList<>();
//        List<String> strings = Arrays.asList(ignoreStr);
//        while (i1.hasNext()) {
//            String key = i1.next();
//            if (strings.contains(key)) {
//                continue;
//            }
//            String v1 = json1.getString(key);
//            String v2 = json2.getString(key);
//            if (!v1.equals(v2)) {
//                FieldRelation fr = new FieldRelation();
//                if(!"MAC".equals(key)){
//                    fr.setSetNum(Double.parseDouble(v2 == null ? "0" : v2));
//                    fr.setActualNum(Double.parseDouble(v1));
//                }
//                fr.setField(key);
//                frList.add(fr);
//            }
//        }
//        Iterator<String> i2 = json2.keySet().iterator();
//        while (i2.hasNext()) {
//            String key = i2.next();
//            if (strings.contains(key)) {
//                continue;
//            }
//            if (!json1.containsKey(key)) {
//                FieldRelation fr = new FieldRelation();
//                fr.setSetNum(Double.parseDouble(json2.getString(key)));
//                fr.setActualNum(0d);
//                fr.setField(key);
//                frList.add(fr);
//            }
//        }
//        return frList;
//    }
//
//}
