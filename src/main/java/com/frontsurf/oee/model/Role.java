package com.frontsurf.oee.model;

import com.frontsurf.oee.common.base.DataEntity;
import com.frontsurf.oee.common.group.Save;
import com.frontsurf.oee.common.group.Update;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import java.util.List;

/**
 * @author liu.zhiyang 2019/5/16 12:31
 * @Email liu.zhiyang@frontsurf.com
 */
public class Role extends DataEntity<Integer> {
    /**
     * name
     * 角色名称
     */
    @NotBlank(message = "角色名称不能为空", groups = {Save.class})
    @Pattern(regexp = "^[a-zA-Z\\u4e00-\\u9fa50-9]{0,6}", groups = {Save.class, Update.class},message = "角色名为汉字+字母+数字组合，长度不能超过6")
    private String name;

    /**
     * code
     * 角色编码
     */
    @NotBlank(message = "角色编码不能为空", groups = {Save.class})
    @Pattern(regexp = "^[a-zA-Z0-9]{0,10}", groups = {Save.class, Update.class},message = "角色编码为字母+数字组合，长度不能超过10")
    private String code;

    /**
     * remarks
     * 角色描述
     */
    private String remarks;



    /**
     * 角色对应的权限
     */
    List<Permission> permissions;









    /**
     * del_flag
     * 删除标志位：0 - 正常；1 - 删除
     */
    private Integer delFlag;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code == null ? null : code.trim();
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks == null ? null : remarks.trim();
    }

    public Integer getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(Integer delFlag) {
        this.delFlag = delFlag;
    }


    public List<Permission> getPermissions() {
        return permissions;
    }

    public void setPermissions(List<Permission> permissions) {
        this.permissions = permissions;
    }
}
