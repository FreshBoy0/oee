package com.frontsurf.oee.model;

import com.frontsurf.oee.common.base.DataEntity;
import com.frontsurf.oee.common.group.Common;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.Date;

public class OeeCausesModels extends DataEntity<Integer> {

    @NotBlank(message = "原因ID不能为空", groups = Common.class)
    private Integer causeId;

    @NotBlank(message = "型号ID不能为空", groups = Common.class)
    private Integer modelId;

    private Integer delFlag;

    public Integer getCauseId() {
        return causeId;
    }

    public void setCauseId(Integer causeId) {
        this.causeId = causeId;
    }

    public Integer getModelId() {
        return modelId;
    }

    public void setModelId(Integer modelId) {
        this.modelId = modelId;
    }

    public Integer getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(Integer delFlag) {
        this.delFlag = delFlag;
    }
}