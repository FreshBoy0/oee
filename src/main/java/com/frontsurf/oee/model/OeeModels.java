package com.frontsurf.oee.model;

import com.frontsurf.oee.common.base.DataEntity;
import com.frontsurf.oee.common.group.Common;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.Date;

public class OeeModels extends DataEntity<Integer> {

    @NotBlank(message = "型号名称不能为空", groups = Common.class)
    private String model;




    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model == null ? null : model.trim();
    }




}