package com.frontsurf.oee.model;

import com.frontsurf.oee.common.base.DataEntity;
import com.frontsurf.oee.common.group.Common;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

public class OeeDevice extends DataEntity<Integer> {

    /**
     * name
     * 设备名称
     */
    @NotBlank(message = "设备名称不能为空", groups = Common.class)
    @Length(max = 12, message = "设备名称长度不能超过12", groups = Common.class)
    private String name;

    /**
     * serial_num
     * 设备编号
     */
//    @NotBlank(message = "设备编号不能为空3", groups = Common.class)
    @Length(max = 12, message = "设备编号长度不能超过12", groups = Common.class)
    private String serialNum;

    /**
     * device_type
     * 设备类型
     */
    @NotBlank(message = "设备类型不能为空", groups = Common.class)

    private String deviceType;
    private int dieChangeTime;
    private  int offLineTime;
    private  int standByTime;
    private  String loadPlan;
    private String deviceBrand;
    private int delFlag;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSerialNum() {
        return serialNum;
    }

    public void setSerialNum(String serialNum) {
        this.serialNum = serialNum;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    public int getDieChangeTime() {
        return dieChangeTime;
    }

    public void setDieChangeTime(int dieChangeTime) {
        this.dieChangeTime = dieChangeTime;
    }

    public int getOffLineTime() {
        return offLineTime;
    }

    public void setOffLineTime(int offLineTime) {
        this.offLineTime = offLineTime;
    }

    public int getStandByTime() {
        return standByTime;
    }

    public void setStandByTime(int standByTime) {
        this.standByTime = standByTime;
    }

    public String getLoadPlan() {
        return loadPlan;
    }

    public void setLoadPlan(String loadPlan) {
        this.loadPlan = loadPlan;
    }

    public String getDeviceBrand() {
        return deviceBrand;
    }

    public void setDeviceBrand(String deviceBrand) {
        this.deviceBrand = deviceBrand;
    }

    public int getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(int delFlag) {
        this.delFlag = delFlag;
    }

    @Override
    public String toString() {
        return "OeeDevice{" +
                "name='" + name + '\'' +
                ", serialNum='" + serialNum + '\'' +
                ", deviceType='" + deviceType + '\'' +
                ", dieChangeTime=" + dieChangeTime +
                ", offLineTime=" + offLineTime +
                ", standByTime=" + standByTime +
                ", loadPlan='" + loadPlan + '\'' +
                ", deviceBrand='" + deviceBrand + '\'' +
                ", delFlag=" + delFlag +
                '}';
    }
}
