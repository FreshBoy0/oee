package com.frontsurf.oee.model;

import com.frontsurf.oee.common.base.DataEntity;

public class OeeTypes extends DataEntity<Integer> {


    private  int type_id;
    private String type_name;
    private int del_flag;

    public int getType_id() {
        return type_id;
    }

    public void setType_id(int type_id) {
        this.type_id = type_id;
    }

    public String getType_name() {
        return type_name;
    }

    public void setType_name(String type_name) {
        this.type_name = type_name;
    }

    public int getDel_flag() {
        return del_flag;
    }

    public void setDel_flag(int del_flag) {
        this.del_flag = del_flag;
    }

    public OeeTypes() {
    }

    @Override
    public String toString() {
        return "OeeTypes{" +
                "type_id=" + type_id +
                ", type_name='" + type_name + '\'' +
                ", del_flag=" + del_flag +
                '}';
    }
}
