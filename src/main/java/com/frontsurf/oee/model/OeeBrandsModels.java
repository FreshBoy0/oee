package com.frontsurf.oee.model;

import com.frontsurf.oee.common.base.DataEntity;
import com.frontsurf.oee.common.group.Common;
import org.apache.ibatis.annotations.Mapper;
import org.hibernate.validator.constraints.Length;
import org.springframework.stereotype.Repository;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.Date;


public class OeeBrandsModels extends DataEntity<Integer> {

    @NotBlank(message = "品牌名称不能为空", groups = Common.class)
    private Integer brandId;

    @NotBlank(message = "型号名称不能为空", groups = Common.class)
    private Integer modelId;


    public Integer getBrandId() {
        return brandId;
    }

    public void setBrandId(Integer brandId) {
        this.brandId = brandId;
    }

    public Integer getModelId() {
        return modelId;
    }

    public void setModelId(Integer modelId) {
        this.modelId = modelId;
    }

}