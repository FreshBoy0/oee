package com.frontsurf.oee.model;

import com.frontsurf.oee.common.base.DataEntity;
import com.frontsurf.oee.common.group.Common;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Date;


public class OeeDeviceLoadScheme extends DataEntity<Integer> {


    /**
     * name
     * 负荷计划名称
     */


    @NotBlank(message = "负荷计划名称不能为空", groups = Common.class)
    private String name;
    /**
     * start_date_time
     * 负荷开始时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
    private Date startDateTime;
    /**
     * end_date_time
     * 负荷结束时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
    private Date endDateTime;

    /**
     * week_start
     * 周重复开始时间
     */
    @Min(value = 1, message = "周重复开始时间不能少于1", groups = Common.class)
    @Max(value = 7, message = "周重复开始时间不能大于7", groups = Common.class)
    @NotNull(message = "周重复开始时间不能为空", groups = Common.class)
    private Integer weekStart;

    /**
     * week_end
     * 周重复结束时间
     */
    @Min(value = 1, message = "周重复结束时间不能少于1", groups = Common.class)
    @Max(value = 7, message = "周重复结束时间不能大于7", groups = Common.class)
    @NotNull(message = "周重复结束时间不能为空", groups = Common.class)
    private Integer weekEnd;

    /**
     * start_date
     * 此模式启用的开始日期
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date startDate;

    /**
     * end_date
     * 此模式启用的截止日期
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date endDate;

    /**
     * rest_time
     * 正常停工的时间列表，逗号隔开
     */
    @NotBlank(message = "正常停工时间列表不能为空", groups = Common.class)
    private String restTime;

    /**
     * del_flag
     * 删除标志位：0 - 正常；1 - 删除
     */
    private Integer delFlag;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getStartDateTime() {
        return startDateTime;
    }

    public void setStartDateTime(Date startDateTime) {
        this.startDateTime = startDateTime;
    }

    public Date getEndDateTime() {
        return endDateTime;
    }

    public void setEndDateTime(Date endDateTime) {
        this.endDateTime = endDateTime;
    }

    public Integer getWeekStart() {
        return weekStart;
    }

    public void setWeekStart(Integer weekStart) {
        this.weekStart = weekStart;
    }

    public Integer getWeekEnd() {
        return weekEnd;
    }

    public void setWeekEnd(Integer weekEnd) {
        this.weekEnd = weekEnd;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getRestTime() {
        return restTime;
    }

    public void setRestTime(String restTime) {
        this.restTime = restTime;
    }

    public Integer getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(Integer delFlag) {
        this.delFlag = delFlag;
    }

    @Override
    public String toString() {
        return "OeeDeviceLoadScheme{" +
                "name='" + name + '\'' +
                ", startDateTime=" + startDateTime +
                ", endDateTime=" + endDateTime +
                ", weekStart=" + weekStart +
                ", weekEnd=" + weekEnd +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                ", restTime='" + restTime + '\'' +
                ", delFlag=" + delFlag +
                '}';
    }
}
