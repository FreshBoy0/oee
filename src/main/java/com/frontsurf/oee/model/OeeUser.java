package com.frontsurf.oee.model;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.frontsurf.oee.common.base.DataEntity;
import com.frontsurf.oee.common.group.Save;
import com.frontsurf.oee.common.group.Update;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class OeeUser extends DataEntity<Integer> implements UserDetails {

    @NotBlank(message = "用户名称不能为空", groups = {Save.class})
    @Pattern(regexp = "^[a-zA-Z\\u4e00-\\u9fa5]{0,9}", groups = {Save.class, Update.class},message = "用户名为汉字+字母组合，长度不能超过9")
    private String name;

    @NotBlank(message = "密码不能为空", groups = {Save.class})
    @JSONField(serialize=false)
//    @Pattern(regexp = "^[a-zA-Z0-9]{0,10}", groups = {Save.class, Update.class},message = "密码为字母和数字组合，长度不能超过10")
    private String password;

    @Pattern(regexp = "^1(3|4|5|7|8)\\d{9}$", message = "手机号码格式错误", groups = {Save.class, Update.class})
    private String phone;

    @NotBlank(message = "系统账号不能为空", groups = {Save.class})
    @Pattern(regexp = "^[a-zA-Z0-9]{0,8}", groups = {Save.class, Update.class},message = "系统账号为数字+字母组合，长度不能超过8")
    private String accountNum;

    /**
     * del_flag
     * 删除标志位：0 - 正常；1 - 删除
     */
    private Integer delFlag;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAccountNum() {
        return accountNum;
    }

    public void setAccountNum(String accountNum) {
        this.accountNum = accountNum;
    }

    public Integer getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(Integer delFlag) {
        this.delFlag = delFlag;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return null;
    }

    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return null;
    }

    @Override
    public boolean isAccountNonExpired() {
        return false;
    }

    @Override
    public boolean isAccountNonLocked() {
        return false;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return false;
    }

    @Override
    public boolean isEnabled() {
        return false;
    }



}