package com.frontsurf.oee.model;

import com.frontsurf.oee.common.base.DataEntity;

public class BrandsTypes extends DataEntity<Integer> {


    private  int brands_types_id;
    private int brand_id;
    private  int type_id;

    public int getBrands_types_id() {
        return brands_types_id;
    }

    public void setBrands_types_id(int brands_types_id) {
        this.brands_types_id = brands_types_id;
    }

    public int getBrand_id() {
        return brand_id;
    }

    public void setBrand_id(int brand_id) {
        this.brand_id = brand_id;
    }

    public int getType_id() {
        return type_id;
    }

    public void setType_id(int type_id) {
        this.type_id = type_id;
    }


    @Override
    public String toString() {
        return "BrandsTypes{" +
                "brands_types_id=" + brands_types_id +
                ", brand_id=" + brand_id +
                ", type_id=" + type_id +
                '}';
    }
}
