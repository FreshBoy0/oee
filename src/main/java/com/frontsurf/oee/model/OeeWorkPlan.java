package com.frontsurf.oee.model;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.frontsurf.oee.common.base.DataEntity;
import com.frontsurf.oee.common.group.Common;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;
@JsonInclude(value = JsonInclude.Include.NON_NULL)
public class OeeWorkPlan extends DataEntity<Integer> {

    @NotBlank(message = "计划名称不能为空", groups = Common.class)
    private String name;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @NotNull(message = "负荷开始不能为空", groups = Common.class)
    private Date startTime;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @NotNull(message = "负荷结束不能为空", groups = Common.class)
    private Date endTime;

    @JsonFormat(pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date startDate;


//    @JSONField()
    @JsonFormat(pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date endDate;

    @Min(value = 1, message = "周重复结束时间不能少于1",groups = Common.class)
    @Max(value = 7, message = "周重复结束时间不能大于7",groups = Common.class)
    @NotNull(message = "开始周期不能为空", groups = Common.class)
    private Integer startWeek;

    @Min(value = 1, message = "周重复开始时间不能少于1",groups = Common.class)
    @Max(value = 7, message = "周重复开始时间不能大于7",groups = Common.class)
    @NotNull(message = "结束周期不能为空", groups = Common.class)
    private Integer endWork;

    private String planDetail;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Integer getStartWeek() {
        return startWeek;
    }

    public void setStartWeek(Integer startWeek) {
        this.startWeek = startWeek;
    }

    public Integer getEndWork() {
        return endWork;
    }

    public void setEndWork(Integer endWork) {
        this.endWork = endWork;
    }

    public String getPlanDetail() {
        return planDetail;
    }

    public void setPlanDetail(String planDetail) {
        this.planDetail = planDetail;
    }

}