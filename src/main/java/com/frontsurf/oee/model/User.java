package com.frontsurf.oee.model;

import com.alibaba.fastjson.annotation.JSONField;
import com.frontsurf.oee.common.base.DataEntity;
import com.frontsurf.oee.common.group.Save;
import com.frontsurf.oee.common.group.Update;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.validation.constraints.*;
import java.util.Collection;
import java.util.List;

/**
 * @author liu.zhiyang 2019/5/16 12:05
 * @Email liu.zhiyang@frontsurf.com
 */
public class User extends DataEntity<Integer> implements UserDetails {



    /**
     * name
     * 用户名称
     */
    @NotBlank(message = "用户名称不能为空", groups = {Save.class})

    @Pattern(regexp = "^[a-zA-Z\\u4e00-\\u9fa5]{0,9}", groups = {Save.class, Update.class},message = "用户名为汉字+字母组合，长度不能超过9")
    private String name;

    /**
     * account_num
     * 系统账号
     */
    @NotBlank(message = "系统账号不能为空", groups = {Save.class})
    @Pattern(regexp = "^[a-zA-Z0-9]{0,8}", groups = {Save.class, Update.class},message = "系统账号为数字+字母组合，长度不能超过8")
    private String accountNum;

    /**
     * organization_id
     * 机构id
     */
    @NotNull(message = "组织机构必选", groups = {Save.class})
    private Integer organizationId;

    /**
     * phone
     * 电话
     */
    @Pattern(regexp = "^1(3|4|5|7|8)\\d{9}$", message = "手机号码格式错误", groups = {Save.class, Update.class})
    private String phone;

    /**
     * email
     * 邮箱
     */
    @Email(message = "邮箱格式错误", groups = {Save.class, Update.class})
    private String email;

    /**
     * 密码
     */
    @NotBlank(message = "密码不能为空", groups = {Save.class})
    @JSONField(serialize=false)
//    @Pattern(regexp = "^[a-zA-Z0-9]{0,10}", groups = {Save.class, Update.class},message = "密码为字母和数字组合，长度不能超过10")
    private String password;

    /**
     * del_flag
     * 删除标志位：0 - 正常；1 - 删除
     */
    private Integer delFlag;

    /**
     * 角色列表
     */
    @NotEmpty(message = "角色必选", groups = {Save.class})
    private List<Role> roles;
    private List<? extends GrantedAuthority> authorities;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAccountNum() {
        return accountNum;
    }

    public void setAccountNum(String accountNum) {
        this.accountNum = accountNum;
    }

    public Integer getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(Integer organizationId) {
        this.organizationId = organizationId;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(Integer delFlag) {
        this.delFlag = delFlag;
    }

    public List<Role> getRoles() {
        return roles;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }
    public void setAuthorities(List<? extends GrantedAuthority> authorities) {
        this.authorities = authorities;
    }
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return this.authorities;
    }

    @Override
    public String getPassword() {
        return this.password;
    }

    @Override
    public String getUsername() {
        return this.name;
    }

    @Override
    @JSONField(serialize=false)
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    @JSONField(serialize=false)
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    @JSONField(serialize=false)
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    @JSONField(serialize=false)
    public boolean isEnabled() {
        return true;
    }
}
