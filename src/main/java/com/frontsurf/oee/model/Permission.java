package com.frontsurf.oee.model;

import com.frontsurf.oee.common.base.DataEntity;

import java.util.List;

/**
 * @author liu.zhiyang 2019/5/16 12:34
 * @Email liu.zhiyang@frontsurf.com
 */
public class Permission extends DataEntity<Integer> {
    /**
     * role_id
     * 角色id
     */
    private Integer roleId;

    /**
     * del_flag
     * 删除标志位：0 - 正常；1 - 删除

     */
    private Integer delFlag;

    private String name;
    private String code;
    private String url;
    private Integer parentId;

    private List<Permission> children;

    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

    public Integer getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(Integer delFlag) {
        this.delFlag = delFlag;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public List<Permission> getChildren() {
        return children;
    }

    public void setChildren(List<Permission> children) {
        this.children = children;
    }
}
