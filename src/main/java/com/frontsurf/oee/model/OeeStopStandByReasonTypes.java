package com.frontsurf.oee.model;

import com.frontsurf.oee.common.base.DataEntity;
import com.frontsurf.oee.common.group.Common;

import javax.validation.constraints.NotBlank;

public class OeeStopStandByReasonTypes extends DataEntity<Integer> {

    private Integer id;
    @NotBlank(message = "原因ID不能为空", groups = Common.class)
    private Integer stop_standby_reason_id;
    @NotBlank(message = "型号ID不能为空", groups = Common.class)
    private Integer stop_standby_type_id;
    private  Integer del_flag;

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getStop_standby_reason_id() {
        return stop_standby_reason_id;
    }

    public void setStop_standby_reason_id(Integer stop_standby_reason_id) {
        this.stop_standby_reason_id = stop_standby_reason_id;
    }

    public Integer getStop_standby_type_id() {
        return stop_standby_type_id;
    }

    public void setStop_standby_type_id(Integer stop_standby_type_id) {
        this.stop_standby_type_id = stop_standby_type_id;
    }

    public Integer getDel_flag() {
        return del_flag;
    }

    public void setDel_flag(Integer del_flag) {
        this.del_flag = del_flag;
    }

    @Override
    public String toString() {
        return "OeeStopStandByReasonTypes{" +
                "id=" + id +
                ", stop_standby_reason_id=" + stop_standby_reason_id +
                ", stop_standby_type_id=" + stop_standby_type_id +
                ", del_flag=" + del_flag +
                '}';
    }
}
