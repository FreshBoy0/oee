package com.frontsurf.oee.model;

import com.frontsurf.oee.common.base.DataEntity;

import java.util.List;

public class OeeBrandtypes extends DataEntity<Integer> {


    private String brand_name;
    private List<OeeTypes>  TypesList;

    public String getBrand_name() {
        return brand_name;
    }

    public void setBrand_name(String brand_name) {
        this.brand_name = brand_name;
    }

    public List<OeeTypes> getTypesList() {
        return TypesList;
    }

    public void setTypesList(List<OeeTypes> typesList) {
        TypesList = typesList;
    }

    @Override
    public String toString() {
        return "OeeBrandtypes{" +
//                "brand_id=" + brand_id +
                ", brand_name='" + brand_name + '\'' +
//                ", del_flag=" + del_flag +
                ", TypesList=" + TypesList +
                '}';
    }

    public OeeBrandtypes() {
    }
}
