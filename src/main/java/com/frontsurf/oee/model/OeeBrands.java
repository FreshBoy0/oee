package com.frontsurf.oee.model;

import com.frontsurf.oee.common.base.DataEntity;

public class OeeBrands extends DataEntity<Integer> {


    private int  brand_id;
    private String brand_name;
    private int del_flag;

    public int getBrand_id() {
        return brand_id;
    }

    public void setBrand_id(int brand_id) {
        this.brand_id = brand_id;
    }

    public String getBrand_name() {
        return brand_name;
    }

    public void setBrand_name(String brand_name) {
        this.brand_name = brand_name;
    }

    public int getDel_flag() {
        return del_flag;
    }

    public void setDel_flag(int del_flag) {
        this.del_flag = del_flag;
    }

    @Override
    public String toString() {
        return "OeeBrands{" +
                "brand_id=" + brand_id +
                ", brand_name='" + brand_name + '\'' +
                ", del_flag=" + del_flag +
                '}';
    }
}

