package com.frontsurf.oee.model;

import com.frontsurf.oee.common.base.DataEntity;
import com.frontsurf.oee.common.group.Common;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.Date;

public class OeeDevices extends DataEntity<Integer> {
    @NotBlank(message = "设备编码不能为空", groups = Common.class)
    private String deviceTag;

    @NotBlank(message = "设备名称不能为空", groups = Common.class)
    private String name;

    @Min(value = 0, message = "不能少于0",groups = Common.class)
    private Integer changeTime;

    @Min(value = 0, message = "不能少于0",groups = Common.class)
    private Integer offlineTime;

    @Min(value = 0, message = "不能少于0",groups = Common.class)
    private Integer standbyTime;

    @NotBlank(message = "品牌名称不能为空", groups = Common.class)
    private String brand;

    @NotBlank(message = "型号不能为空", groups = Common.class)
    private String model;

    @NotBlank(message = "计划不能为空", groups = Common.class)
    private String plan;



    public String getDeviceTag() {
        return deviceTag;
    }

    public void setDeviceTag(String deviceTag) {
        this.deviceTag = deviceTag == null ? null : deviceTag.trim();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public Integer getChangeTime() {
        return changeTime;
    }

    public void setChangeTime(Integer changeTime) {
        this.changeTime = changeTime;
    }

    public Integer getOfflineTime() {
        return offlineTime;
    }

    public void setOfflineTime(Integer offlineTime) {
        this.offlineTime = offlineTime;
    }

    public Integer getStandbyTime() {
        return standbyTime;
    }

    public void setStandbyTime(Integer standbyTime) {
        this.standbyTime = standbyTime;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand == null ? null : brand.trim();
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model == null ? null : model.trim();
    }

    public String getPlan() {
        return plan;
    }

    public void setPlan(String plan) {
        this.plan = plan == null ? null : plan.trim();
    }

}