package com.frontsurf.oee.model;

import com.frontsurf.oee.common.base.DataEntity;
import com.frontsurf.oee.common.group.Common;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class OeeWaitCauses extends DataEntity<Integer> {

    @NotNull(message = "时间类别不能为空", groups = Common.class)
    private Integer timeId;

    @NotNull(message = "原因类别不能为空", groups = Common.class)
    private Integer matterId;

    @NotBlank(message = "原因细节不能为空", groups = Common.class)
    private String causeDetail;

    @NotEmpty(message = "型号表不能为空", groups = Common.class)
    private List<OeeModels> oeeModels;



    public Integer getTimeId() {
        return timeId;
    }

    public void setTimeId(Integer timeId) {
        this.timeId = timeId;
    }

    public Integer getMatterId() {
        return matterId;
    }

    public void setMatterId(Integer matterId) {
        this.matterId = matterId;
    }

    public String getCauseDetail() {
        return causeDetail;
    }

    public void setCauseDetail(String causeDetail) {
        this.causeDetail = causeDetail;
    }

    public List<OeeModels> getOeeModels() {
        return oeeModels;
    }

    public void setOeeModels(List<OeeModels> oeeModels) {
        this.oeeModels = oeeModels;
    }

}