package com.frontsurf.oee.model;

import com.frontsurf.oee.common.base.DataEntity;
import com.frontsurf.oee.common.group.Common;

import javax.validation.constraints.NotNull;
import java.util.List;

public class OeeStopStandbyReasons extends DataEntity<Integer> {


    private Integer id;
    @NotNull(message = "时间类别不能为空", groups = Common.class)
    private Integer time_category_num;
    @NotNull(message = "原因类别不能为空", groups = Common.class)
    private Integer cause_category_num;
    @NotNull(message = "原因细节不能为空", groups = Common.class)
    private  String cause_detail;
    @NotNull(message = "型号不能为空", groups = Common.class)
    private List<OeeTypes> oeeTypes;
    private  Integer del_flag;


    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getTime_category_num() {
        return time_category_num;
    }

    public void setTime_category_num(Integer time_category_num) {
        this.time_category_num = time_category_num;
    }

    public Integer getCause_category_num() {
        return cause_category_num;
    }

    public void setCause_category_num(Integer cause_category_num) {
        this.cause_category_num = cause_category_num;
    }

    public String getCause_detail() {
        return cause_detail;
    }

    public void setCause_detail(String cause_detail) {
        this.cause_detail = cause_detail;
    }

    public List<OeeTypes> getOeeTypes() {
        return oeeTypes;
    }

    public void setOeeTypes(List<OeeTypes> oeeTypes) {
        this.oeeTypes = oeeTypes;
    }

    public Integer getDel_flag() {
        return del_flag;
    }

    public void setDel_flag(Integer del_flag) {
        this.del_flag = del_flag;
    }


    @Override
    public String toString() {
        return "OeeStopStandbyReasons{" +
                "id=" + id +
                ", time_category_num=" + time_category_num +
                ", cause_category_num=" + cause_category_num +
                ", cause_detail='" + cause_detail + '\'' +
                ", oeeTypes=" + oeeTypes +
                ", del_flag=" + del_flag +
                '}';
    }
}
