package com.frontsurf.oee.mapper.deviceManage;

import com.frontsurf.oee.model.OeeStopStandbyReasons;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectKey;
import org.springframework.stereotype.Repository;

@Repository
public interface OeeStopStandByReasonsMapper {


    /**
     * 插入一条记录
     * @param record
     * @return
     */
    @Insert({
            "insert into oee_stop_standby_reasons (time_category_num, cause_category_num, cause_detail)",
            "values (#{time_category_num,jdbcType=INTEGER}, #{cause_category_num,jdbcType=INTEGER}, #{cause_detail,jdbcType=VARCHAR})"
    })
    @SelectKey(statement="SELECT LAST_INSERT_ID()", keyProperty="id", before=false, resultType=Integer.class)
    int insert(OeeStopStandbyReasons record);




}
