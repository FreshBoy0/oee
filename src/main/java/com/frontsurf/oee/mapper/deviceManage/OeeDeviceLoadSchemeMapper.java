package com.frontsurf.oee.mapper.deviceManage;

import com.frontsurf.oee.model.OeeDeviceLoadScheme;
import com.frontsurf.oee.util.Return;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.type.JdbcType;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OeeDeviceLoadSchemeMapper {


    /**
     * 按id 查詢负荷计划表的信息
     *
     * @param id
     * @return
     */
    @Select("SELECT * FROM oee_device_load_scheme WHERE id=#{id} AND del_flag=0")
    OeeDeviceLoadScheme queryOeeDeviceLoadSchemeById(Integer id);
    /**
     * 按name查詢负荷计划表的信息
     *
     * @param name
     * @return
     */
    @Select("SELECT * FROM oee_device_load_scheme WHERE name=#{name} AND del_flag=0")
    OeeDeviceLoadScheme queryOeeDeviceLoadSchemeByName(String name);

    /**
     * 查询所有的设备负荷计划信息
     * @return
     */
    @Select("SELECT * FROM oee_device_load_scheme ")
    List<OeeDeviceLoadScheme> queryAllDeviceLoadScheme();

    /**
     * 更新设备的工作计划信息
     *
     * @param
     * @return
     */


    @Update({

            "update oee_device_load_scheme",
            "set name = #{name, jdbcType =VARCHAR},",
            "start_date_time = #{startDateTime, jdbcType =TIMESTAMP},",
            "end_date_time = #{endDateTime, jdbcType =TIMESTAMP},",
            "week_start = #{weekStart, jdbcType =INTEGER},",
            "week_end = #{weekEnd, jdbcType =INTEGER},",
            "start_date = #{startDate, jdbcType =DATE},",
            "end_date = #{endDate, jdbcType =DATE},",
            "rest_time = #{restTime , jdbcType =VARCHAR},",
            "del_flag = #{delFlag, jdbcType =INTEGER}",
            "where id = #{id,jdbcType =INTEGER}"
            })
    Integer updateOeeDeviceLoadSchemeById(OeeDeviceLoadScheme record);



    /**
     * 删除设备负荷计划信息
     *
     * @param id
     * @return
     */
    @Update("UPDATE oee_device_load_scheme SET del_flag=1 WHERE id=#{id}")
    Integer deleteDeviceLoadSchemeById(int id);
    /**
     * 录入设备的负荷计划信息
     *
     * @param record
     * @return
     */
    @Insert({
            "insert into oee_device_load_scheme (name, start_date_time,end_date_time,week_start, week_end, start_date, end_date, rest_time, del_flag)",
            "values (#{name,jdbcType=VARCHAR},#{startDateTime,jdbcType=TIMESTAMP},#{endDateTime,jdbcType=TIMESTAMP}, #{weekStart,jdbcType=INTEGER}, ",
            "#{weekEnd,jdbcType=INTEGER}, #{startDate,jdbcType=DATE},#{endDate,jdbcType=DATE}, #{restTime,jdbcType=VARCHAR}, #{delFlag,jdbcType=INTEGER})"
//            "#{createBy,jdbcType=INTEGER},#{createName,jdbcType=VARCHAR},#{createDate,jdbcType=DATE},#{updateBy,jdbcType=INTEGER},#{updateName,jdbcType=VARCHAR},#{updateDate,jdbcType=DATE}"

    })
    Integer insert(OeeDeviceLoadScheme record);





}
