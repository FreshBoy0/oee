package com.frontsurf.oee.mapper.deviceManage;

import com.frontsurf.oee.model.OeeDevice;
import com.frontsurf.oee.model.OeeDeviceLoadScheme;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.type.JdbcType;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface OeeDeviceMapper {

    
    /**
     * 录入设备信息
     *
     * @param record
     * @return
     */   
    @Insert({
            "insert into oee_device (name, serial_num, device_type, die_change_time, offline_time, standby_time, del_flag,load_plan, device_brand)",
            "values (#{name,jdbcType=VARCHAR}, #{serialNum,jdbcType=VARCHAR}, #{deviceType,jdbcType=VARCHAR}, #{dieChangeTime,jdbcType=INTEGER}, ",
            "#{offLineTime,jdbcType=INTEGER}, #{standByTime,jdbcType=INTEGER}, #{delFlag,jdbcType=INTEGER}, #{loadPlan,jdbcType=VARCHAR}, #{deviceBrand,jdbcType=VARCHAR})"
    })
    int insert(OeeDevice record);

    /**
     * 更新设备信息
     *
     * @param record
     * @return
     */


    @Update({

            "update oee_device",
            "set name = #{name, jdbcType =VARCHAR},",
//            "id = #{id,jdbcType= INTEGER},",
            "serial_num = #{serialNum, jdbcType =VARCHAR},",
            "device_type = #{deviceType, jdbcType =VARCHAR},",
            "die_change_time = #{dieChangeTime, jdbcType =INTEGER},",
            "offline_time = #{offLineTime, jdbcType =INTEGER},",
            "standby_time = #{standByTime, jdbcType =INTEGER},",
            "load_plan= #{loadPlan , jdbcType =VARCHAR },",
            "del_flag = #{delFlag, jdbcType =INTEGER},",
            "device_brand = #{deviceBrand , jdbcType =VARCHAR}  ",
            "where id = #{id,jdbcType =INTEGER}"
    })
    Integer updateDeviceById(OeeDevice record);
    /**
     * 根据id查询设备信息
     *
     * @param id
     * @return
     */
    @Select("select * from oee_device where id=#{id} and del_flag=0")
    OeeDevice queryDeviceById(Integer id);
    /**
     * 根据设备编号查询设备信息
     *
     * @param
     * @return
     */
    @Select("select * from oee_device where serial_num=#{serialNum} and del_flag=0")
    OeeDevice queryDeviceBySerialNum(String serialNum);

    /**
     * 根据品牌查询设备信息
     *
     * @return
     */
    @Select("select * from oee_device where device_brand=#{brand} and del_flag=0")
    List<OeeDevice> queryDeviceByBrand(String brand);
    /**
     * 根据设备名查询设备信息
     *
     * @param name
     * @return
     */
    @Select("select * from oee_device where name=#{name} and del_flag=0")
    List<OeeDevice> queryDeviceByName(String name);
    /**
     *
     * 查询所有设备信息
     */
    @Select("select * from oee_device where del_flag=0")
    List<OeeDevice> queryAllDevice();
    /**
     * 根据id删除设备数据
     *
     * @param id
     * @return
     */
    @Update("UPDATE oee_device SET del_flag=1 WHERE id=#{id}")
    int deleteDeviceById(Integer id);







    
}
