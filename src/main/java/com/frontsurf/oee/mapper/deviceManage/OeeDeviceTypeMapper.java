package com.frontsurf.oee.mapper.deviceManage;

import com.frontsurf.oee.model.OeeTypes;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

public interface OeeDeviceTypeMapper {


    /**
     * 录入设备型号信息
     *
     * @param name
     * @return
     */
    @Insert({
            "insert into oee_types (type_name)",
            "values (#{type_name,jdbcType=VARCHAR})"
    })
    int insert(String name);


    /**
     * 根据型号名name查询型号信息
     *
     * @param type_name
     * @return
     */
    @Select("select count(*)  from oee_types where type_name=#{type_name} and del_flag=0")
    int  queryTypeByName(String type_name);
    /**
     * 根据型号名name查询型号ID
     *
     * @param name
     * @return
     */
    @Select("select  type_id   from oee_types where type_name=#{type_name} and del_flag=0")
    int  queryTypeIdByName(String name);

    /**
     * 查询所有的型号
     *
     * @return
     */
    @Select("select * from oee_types where del_flag=0")
    List<OeeTypes> queryAllType();

    /**
     * 根据id删除型号信息
     *
     * @param id
     * @return
     */
    @Update("UPDATE oee_types SET del_flag=1 WHERE type_id=#{type_id}")
    int deleteTypeById(Integer id);
    /**
     * 根据id更新型号信息
     *
     * @param
     * @return
     */

    @Update({
            "update oee_types",
            "set name = #{name, jdbcType =VARCHAR} ",
            "where id = #{id,jdbcType =INTEGER}"
    })
    Integer updateTypeById(OeeTypes record);

    /**
     * 根据型号id查找型号名
     *
     * @param
     * @return
     */
    @Select("select type_name from oee_types where type_id=#{type_id} and del_flag=0")
    String queryTypeNameById(int type_id);


}
