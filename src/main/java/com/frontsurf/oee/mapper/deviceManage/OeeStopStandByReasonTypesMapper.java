package com.frontsurf.oee.mapper.deviceManage;

import com.frontsurf.oee.model.OeeStopStandbyReasons;
import com.frontsurf.oee.model.OeeTypes;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

@Repository
public interface OeeStopStandByReasonTypesMapper {

    /**
     * 插入一条记录
     * @param
     * @return
     */
    @Insert({
            "insert into oee_stop_standby_reason_types (stop_standby_reason_id, type_id)",
            "values (#{id,jdbcType=INTEGER}, #{typeID,jdbcType=INTEGER})"
    })
    int insert(Integer id , Integer typeID);















}
