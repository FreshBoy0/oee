package com.frontsurf.oee.mapper.deviceManage;

import com.frontsurf.oee.model.OeeBrands;
import com.frontsurf.oee.model.OeeDevice;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface OeeDeviceBrandMapper {

    /**
     * 录入厂商品牌信息
     *
     * @param
     * @return
     */
    @Insert({
            "insert into oee_brands (brand_name)",
            "values (#{brand_name,jdbcType=VARCHAR})"
    })
    int insert(String brand_name);


    /**
     * 根据name查询厂商信息
     *
     * @param brand_name
     * @return
     */
    @Select("select count(*)  from oee_brands where brand_name=#{brand_name} and del_flag=0")
    int  queryBrandByName(String brand_name);


//    @Select("select * from oee_device where id=#{id} and del_flag=0")
//    OeeDevice queryDeviceById(Integer id);




    /**
     * 根据name查询品牌id
     *
     * @param brand_name
     * @return
     */
    @Select("select  brand_id  from oee_brands where brand_name = #{brand_name} and del_flag=0")
    int queryBrandIdByName(String brand_name);

    /**
     * 查询所有的厂商
     *
     * @return
     */
    @Select("select * from oee_brands where del_flag=0")
    List<OeeBrands> queryAllBrand();

    /**
     * 删除厂商信息
     *
     * @param id
     * @return
     */
    @Update("UPDATE oee_brands SET del_flag=1 WHERE id=#{id}")
    int deleteBrandById(Integer id);


    @Update({
            "update oee_brands",
            "set brand_name = #{brand_name, jdbcType =VARCHAR} ",
            "where id = #{id,jdbcType =INTEGER}"
    })
    Integer updateBrandById(OeeBrands record);



}
