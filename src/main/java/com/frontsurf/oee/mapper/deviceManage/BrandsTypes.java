package com.frontsurf.oee.mapper.deviceManage;

import com.frontsurf.oee.model.OeeTypes;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface BrandsTypes {


    /**
     * 录入品牌型号关联信息
     *
     * @param
     * @return
     */
    @Insert({
            "insert into brands_types (brand_id,type_id)",
            "values (#{brand_id,jdbcType=INTEGER},#{ type_id , jdbcType=INTEGER })"
    })
    int insert(int brand_id , int type_id );
    /**
     * 根据输入的品牌id查询对应的型号id
     *
     * @param
     * @return
     */
    @Select({
            "select type_id from brands_types where  brand_id=#{brand_id}  and del_flag=0"
    })
    List<Integer> selectTypesIdByBrandId(int brandId);

    /**
     *根据品牌id删除关联表的信息
     * @param brand_id
     */
    @Delete("update brands_types Set del_flag=1 where brand_id=#{brand_id}")
    void deleteByBrandID(Integer brand_id);
    /**
     *根据品牌id查找关联表中的型号id信息
     * @param id
     */
    @Select("select * from brands_types where del_flag=0 and brand_id=#{id}")
    List<BrandsTypes> selectTypeIDByBrandID(Integer id);



    /**
     *根据品牌id查询关联表的信息
     * @param id
     */
    @Select("select * from oee_brands_models where del_flag=0 and brand_id=#{id}")
    List<OeeTypes> selectByBrandID(Integer id);







}
