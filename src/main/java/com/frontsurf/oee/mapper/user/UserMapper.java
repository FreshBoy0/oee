package com.frontsurf.oee.mapper.user;

import com.frontsurf.oee.model.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author liu.zhiyang 2019/5/16 13:32
 * @Email liu.zhiyang@frontsurf.com
 */
@Mapper
@Repository
public interface UserMapper {

    @Select("SELECT * FROM device_oee_user WHERE del_flag=0 AND account_num=#{accountNum}")
   User selectByAccountNum(String accountNum);

    @Select("SELECT * FROM device_oee_user WHERE del_flag=0")
    List<User> userList();

    @Select("SELECT * FROM device_oee_user WHERE del_flag=0 AND id=#{userId};")
    User getUserById(Integer userId);
}
