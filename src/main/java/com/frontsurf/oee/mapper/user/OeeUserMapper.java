package com.frontsurf.oee.mapper.user;

import com.frontsurf.oee.model.OeeUser;
import java.util.List;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectKey;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.type.JdbcType;

public interface OeeUserMapper {
    @Delete({
        "delete from oee_user",
        "where id = #{id,jdbcType=INTEGER}"
    })
    int deleteByPrimaryKey(Integer id);

    @Insert({
        "insert into oee_user (`name`, `password`, ",
        "phone, del_flag, account_num)",
        "values (#{name,jdbcType=VARCHAR}, #{password,jdbcType=VARCHAR}, ",
        "#{phone,jdbcType=CHAR}, #{delFlag,jdbcType=INTEGER}, #{createName,jdbcType=VARCHAR})"
    })
    int insert(OeeUser record);

    @Select({
        "select",
        "id, `name`, `password`, phone, del_flag, create_by, create_name, create_date, ",
        "update_by, update_date, account_num",
        "from oee_user",
        "where id = #{id,jdbcType=INTEGER}"
    })
    @Results({
        @Result(column="id", property="id", jdbcType=JdbcType.INTEGER, id=true),
        @Result(column="name", property="name", jdbcType=JdbcType.VARCHAR),
        @Result(column="password", property="password", jdbcType=JdbcType.VARCHAR),
        @Result(column="phone", property="phone", jdbcType=JdbcType.CHAR),
        @Result(column="del_flag", property="delFlag", jdbcType=JdbcType.BIT),
        @Result(column="create_by", property="createBy", jdbcType=JdbcType.INTEGER),
        @Result(column="create_name", property="createName", jdbcType=JdbcType.VARCHAR),
        @Result(column="create_date", property="createDate", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="update_by", property="updateBy", jdbcType=JdbcType.INTEGER),
        @Result(column="update_date", property="updateDate", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="account_num", property="accountNum", jdbcType=JdbcType.VARCHAR)
    })
    OeeUser selectByPrimaryKey(Integer id);

    @Select({
        "select",
        "id, `name`, `password`, phone, del_flag, create_by, create_name, create_date, ",
        "update_by, update_date, account_num",
        "from oee_user"
    })
    @Results({
        @Result(column="id", property="id", jdbcType=JdbcType.INTEGER, id=true),
        @Result(column="name", property="name", jdbcType=JdbcType.VARCHAR),
        @Result(column="password", property="password", jdbcType=JdbcType.VARCHAR),
        @Result(column="phone", property="phone", jdbcType=JdbcType.CHAR),
        @Result(column="del_flag", property="delFlag", jdbcType=JdbcType.BIT),
        @Result(column="create_by", property="createBy", jdbcType=JdbcType.INTEGER),
        @Result(column="create_name", property="createName", jdbcType=JdbcType.VARCHAR),
        @Result(column="create_date", property="createDate", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="update_by", property="updateBy", jdbcType=JdbcType.INTEGER),
        @Result(column="update_date", property="updateDate", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="account_num", property="accountNum", jdbcType=JdbcType.VARCHAR)
    })
    List<OeeUser> selectAll();

    @Update({
        "update oee_user",
        "set `name` = #{name,jdbcType=VARCHAR},",
          "`password` = #{password,jdbcType=VARCHAR},",
          "phone = #{phone,jdbcType=CHAR},",
          "del_flag = #{delFlag,jdbcType=BIT},",
          "create_by = #{createBy,jdbcType=INTEGER},",
          "create_name = #{createName,jdbcType=VARCHAR},",
          "create_date = #{createDate,jdbcType=TIMESTAMP},",
          "update_by = #{updateBy,jdbcType=INTEGER},",
          "update_date = #{updateDate,jdbcType=TIMESTAMP},",
          "account_num = #{accountNum,jdbcType=VARCHAR}",
        "where id = #{id,jdbcType=INTEGER}"
    })
    int updateByPrimaryKey(OeeUser record);
}