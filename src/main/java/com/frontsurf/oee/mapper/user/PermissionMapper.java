package com.frontsurf.oee.mapper.user;

        import com.frontsurf.oee.model.Permission;
        import org.apache.ibatis.annotations.Mapper;
        import org.apache.ibatis.annotations.Select;
        import org.springframework.stereotype.Repository;

        import java.util.List;

/**
 * @author liu.zhiyang 2019/5/16 13:21
 * @Email liu.zhiyang@frontsurf.com
 */
@Mapper
@Repository
public interface PermissionMapper {



    /**
     * 查找子级权限
     *mybatis
     * @param parentId
     * @return
     */
    @Select("SELECT * FROM device_oee_permission  WHERE  del_flag=0  AND parent_id=#{parentId}")
    List<Permission> selectByParentId(Integer parentId);

    /**
     * 根据id查找权限
     * @param Id
     * @return
     */
    @Select("SELECT * FROM device_oee_permission  WHERE  del_flag=0  AND id=#{id}")
    Permission selectById(Integer Id);

}
