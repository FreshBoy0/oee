package com.frontsurf.oee.mapper.user;


import com.frontsurf.oee.model.Role;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface RoleMapper {

    @Select("SELECT * FROM device_oee_role where del_flag =0 and id in (select role_id from device_oee_user_role where del_flag=0  and user_id = #{Id})")
    List<Role> selectRoleByUserId(Integer Id);

}
