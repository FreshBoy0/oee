package com.frontsurf.oee.security.handler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @Author xu.xiaojing
 * @Date 2018/10/10 9:25
 * @Email xu.xiaojing@frontsurf.com
 * @Description
 */
@Component
public class CustomAuthenticationFailHandler extends SimpleUrlAuthenticationFailureHandler {

   static   Logger logger = LoggerFactory.getLogger(CustomAuthenticationFailHandler.class);

    @Override
    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception) throws IOException, ServletException {
        logger.debug("身份验证失败");
//        super.onAuthenticationFailure(request, response, exception);
        String message = exception.getMessage();
        if(message.equalsIgnoreCase("Bad credentials")){
            message = "用户名或密码错误！";
        }
        request.getRequestDispatcher("/login/fail?message="+message).forward(request, response);
    }
}
