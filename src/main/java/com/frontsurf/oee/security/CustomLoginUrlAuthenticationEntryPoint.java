package com.frontsurf.oee.security;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @Author xu.xiaojing
 * @Date 2019/4/2 11:04
 * @Email xu.xiaojing@frontsurf.com
 * @Description
 */

public class CustomLoginUrlAuthenticationEntryPoint implements AuthenticationEntryPoint {
    @Override
    public void commence(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, AuthenticationException e) throws IOException, ServletException {

        httpServletRequest.getRequestDispatcher("/login/access/unauthenticated").forward(httpServletRequest,httpServletResponse);
    }
}
