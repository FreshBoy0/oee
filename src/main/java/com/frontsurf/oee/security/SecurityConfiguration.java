package com.frontsurf.oee.security;


import com.frontsurf.oee.security.handler.CustomAccessDeniedHandler;
import com.frontsurf.oee.security.handler.CustomAuthenticationFailHandler;
import com.frontsurf.oee.security.handler.CustomAuthenticationSuccesstHandler;
import com.frontsurf.oee.security.handler.CustomLogoutSuccessHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.access.intercept.FilterSecurityInterceptor;

/**
 * @Author xu.xiaojing
 * @Date 2018/9/29 23:47
 * @Email xu.xiaojing@frontsurf.com
 * @Description 这是 Spring Security 必须配置的类,是Spring Security基础的配置类
 */

@Configuration
@EnableWebSecurity //
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Autowired
    UrlUserDetailsService urlUserDetailsService;

    @Autowired
    CustomFilterSecurityInterceptor urlFilterSecurityInterceptor;

    @Autowired
    CustomAuthenticationSuccesstHandler successtHandler;

/*    @Resource(name = "customObjectPostProcessor")
    CustomObjectPostProcessor customObjectPostProcessor;*/

    @Autowired
    CustomSecurityMetadataSource metadataSource;

    @Autowired
    CustomAccessDecisionManager accessDecisionManager;

    /**
     * Http 安全
     * HttpSecurity用于提供一系列的Security默认的Filter，最终在WebSecurity对象中，组装到最终产生的springSecurityFilterChain 对象中去；
     * 用于配置资源URL的安全性,设置资源的过滤器，
     *
     * @param http
     * @throws Exception
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http.formLogin().loginProcessingUrl("/login").usernameParameter("username").passwordParameter("password").permitAll().successHandler(successtHandler).failureHandler(new CustomAuthenticationFailHandler());
        http.logout().logoutUrl("/login/logout").logoutSuccessHandler(new CustomLogoutSuccessHandler());
        http.csrf().disable();
        http.cors().disable();
        http.authorizeRequests().antMatchers("/login", "/static/**", "/error", "/login/**","/websocket/**").permitAll();
//        //华为云的四个接口
//        http.authorizeRequests().antMatchers("/hicloud/*").permitAll();
        http.authorizeRequests().anyRequest().authenticated();

        http.exceptionHandling().accessDeniedHandler(new CustomAccessDeniedHandler()).authenticationEntryPoint(new CustomLoginUrlAuthenticationEntryPoint());

     /*   http.authorizeRequests().withObjectPostProcessor(new ObjectPostProcessor<FilterSecurityInterceptor>() {
            @Override
            public <O extends FilterSecurityInterceptor> O postProcess(O obj) {
                obj.setSecurityMetadataSource(metadataSource);
                obj.setAccessDecisionManager(accessDecisionManager);
                return obj;
            }
        });*/
        http.addFilterAfter(urlFilterSecurityInterceptor, FilterSecurityInterceptor.class);
    }

    /**
     * WEB 安全
     * WebSecurity 配置的是全局性，配置全局的过滤器，如 忽略某些资源，拒绝某些请求，设置调试模式 等
     * 所以 设置的过滤器 是要比 HttpSecurity 优先级高
     *
     * @param web
     * @throws Exception
     */
    @Override
    public void configure(WebSecurity web) throws Exception {
        super.configure(web);
    }

    /**
     * AuthenticationManagerBuilder 是身份认证管理生成器，用于生成身份认证的创建机制
     *
     * @param auth
     * @throws Exception
     */
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {

//        auth.userDetailsService(urlUserDetailsService).passwordEncoder(new BCryptPasswordEncoder());
        DaoAuthenticationProvider daoAuthenticationProvider = new DaoAuthenticationProvider();
        daoAuthenticationProvider.setHideUserNotFoundExceptions(false);
        daoAuthenticationProvider.setUserDetailsService(urlUserDetailsService);
        daoAuthenticationProvider.setPasswordEncoder(new BCryptPasswordEncoder());
        auth.authenticationProvider(daoAuthenticationProvider);
    }


    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

}
