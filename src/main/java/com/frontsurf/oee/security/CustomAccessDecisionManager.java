package com.frontsurf.oee.security;

import com.frontsurf.oee.security.model.UrlConfigAttribute;
import com.frontsurf.oee.security.model.UrlGrantedAuthority;
import org.springframework.security.access.AccessDecisionManager;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.ConfigAttribute;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.Collection;

/**
 * @Author xu.xiaojing
 * @Date 2018/10/8 9:57
 * @Email xu.xiaojing@frontsurf.com
 * @Description
 */

@Component
public class CustomAccessDecisionManager implements AccessDecisionManager {


    @Override
    public void decide(Authentication authentication, Object object, Collection<ConfigAttribute> configAttributes) throws AccessDeniedException, InsufficientAuthenticationException {

//      if(true)
//          return;

        if (configAttributes != null) {

            for (ConfigAttribute configAttribute : configAttributes) {
                UrlConfigAttribute urlConfigAttribute = (UrlConfigAttribute) configAttribute;
                for (GrantedAuthority grantedAuthority : authentication.getAuthorities()) {
                    UrlGrantedAuthority urlGrantedAuthority = (UrlGrantedAuthority)grantedAuthority;
                    if(urlGrantedAuthority.getPermissionUrl().equals(urlConfigAttribute.getUri())){
                        return;
                    }
                }
            }
        }


/*       String url, method;

        // 资源权限集合
        for (ConfigAttribute ca : configAttributes) {
            // 用户的权限集合
            for (GrantedAuthority ga : authentication.getAuthorities()) {
                //处理未登录的用户，即游客
                if (authentication.getPrincipal().equals("anonymousUser")) {
                    throw new AccessDeniedException(this.messages.getMessage("AbstractAccessDecisionManager.accessDenied", "Access is denied"));
                }
                UrlGrantedAuthority cga = (UrlGrantedAuthority) ga;
                url = cga.getPermissionUrl();
                if (url == null || url.equals("")) {
                    continue;
                }
                method = cga.getMethod();
                // 判断路径权限
                if (matchers(url, request)) {
                    // 判断方法是否正确
                    String method1 = request.getMethod();
                    if (request.getMethod().equals(method) || "ALL".equals(method)) {
                        return;
                    }
                }
            }
        }*/

        throw new AccessDeniedException("no right");
    }

    @Override
    public boolean supports(ConfigAttribute configAttribute) {
        return true;
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return true;
    }

    private boolean matchers(String url, HttpServletRequest request) {
        AntPathRequestMatcher matcher = new AntPathRequestMatcher(url);
        if (matcher.matches(request)) {
            return true;
        }
        return false;
    }
}
