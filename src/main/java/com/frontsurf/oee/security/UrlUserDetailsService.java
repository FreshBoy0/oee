package com.frontsurf.oee.security;


import com.frontsurf.oee.mapper.user.RoleMapper;
import com.frontsurf.oee.model.Role;
import com.frontsurf.oee.model.User;
import com.frontsurf.oee.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @Author xu.xiaojing
 * @Date 2018/9/29 23:30
 * @Email xu.xiaojing@frontsurf.com
 * @Description
 */

@Component
public class UrlUserDetailsService implements UserDetailsService {

    @Autowired
    UserService userService;

    @Autowired
    RoleMapper roleMapper;

    @Override
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
        // 从数据库获取用户
        // 此处之所以不用校验密码，是因为Spring 自动校验，特别是 User继承自 UserDetail

        User user = userService.getUserByAccountNum(userName);
        if (user == null) {
            throw new UsernameNotFoundException("用户名或密码错误！");
        }

        List<Role> roles = roleMapper.selectRoleByUserId(user.getId());
        if (roles != null && roles.size() >0){
            user.setRoles(roles);
        }

        return user;

    }

//    private void transferPermissions(MesWebPermission permission, Set<UrlGrantedAuthority> grantedAuthoritySet) {
//        if (permission == null) {
//            return;
//        }
//        //处理本级权限
//        if (permission.getApiPermissions() != null) {
//            for (MesApiPermission apiPermission : permission.getApiPermissions()) {
//                UrlGrantedAuthority urlGrantedAuthority = new UrlGrantedAuthority(apiPermission.getUrl(), apiPermission.getMethod(), permission.getCode());
//                grantedAuthoritySet.add(urlGrantedAuthority);
//            }
//        }
//        //处理本级权限下的所有子级权限
//        for (MesWebPermission child : permission.getChildren()) {
//            this.transferPermissions(child, grantedAuthoritySet);
//        }
//    }
}


