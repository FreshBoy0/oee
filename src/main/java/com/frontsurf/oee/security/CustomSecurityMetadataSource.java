package com.frontsurf.oee.security;

import org.springframework.security.access.ConfigAttribute;
import org.springframework.security.web.FilterInvocation;
import org.springframework.security.web.access.intercept.FilterInvocationSecurityMetadataSource;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;
import org.springframework.stereotype.Component;

import java.util.*;

/**
 * @Author xu.xiaojing
 * @Date 2019/4/2 0:06
 * @Email xu.xiaojing@frontsurf.com
 * @Description
 */
@Component
public class CustomSecurityMetadataSource implements FilterInvocationSecurityMetadataSource {

//    @Autowired
//    MesPermissionService mesPermissionService;

//    @Value("${tenant.admin.permissions}")
//    String tenantPermissionStr;

    Map<String, Collection<ConfigAttribute>> medataSources = new HashMap<>();

    @Override
    public Collection<ConfigAttribute> getAttributes(Object o) throws IllegalArgumentException {
        FilterInvocation fi = (FilterInvocation) o;
        if(medataSources.size() <= 0){
            getAllConfigAttributes();
        }
        for (Map.Entry<String, Collection<ConfigAttribute>> entry : medataSources.entrySet()) {
            String uri = entry.getKey();
            RequestMatcher requestMatcher = new AntPathRequestMatcher(uri);
            if (requestMatcher.matches(fi.getHttpRequest())) {
                return entry.getValue();
            }
        }
        return null;
    }

    @Override
    public Collection<ConfigAttribute> getAllConfigAttributes() {
//
//
//        List<MesApiPermission> apiPermissions = mesPermissionService.getAllApiPermission();
//        if (apiPermissions != null) {
//            for (MesApiPermission permission : apiPermissions) {
//                ArrayList<ConfigAttribute> configAttributes = new ArrayList<>();
//                if(StringUtils.isNotBlank(permission.getUrl())){
//                    UrlConfigAttribute urlConfigAttribute = new UrlConfigAttribute(permission.getUrl(), permission.getMethod());
//                    configAttributes.add(urlConfigAttribute);
//                }
//                medataSources.put(permission.getUrl(), configAttributes);
//            }
//        }
//        //租户管理员权限
//        if(StringUtils.isNotBlank(tenantPermissionStr)){
//            String[] permissions = tenantPermissionStr.split(",");
//            for (int i = 0; i < permissions.length; i++) {
//                if(StringUtils.isNotBlank(permissions[i])){
//                    ArrayList<ConfigAttribute> configAttributes = new ArrayList<>();
//                    UrlConfigAttribute urlConfigAttribute = new UrlConfigAttribute(permissions[i],null);
//                    configAttributes.add(urlConfigAttribute);
//                    medataSources.put(permissions[i],configAttributes);
//                }
//            }
//        }

        return null;
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return true;
    }
}
