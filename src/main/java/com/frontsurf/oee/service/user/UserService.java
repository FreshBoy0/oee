package com.frontsurf.oee.service.user;

import com.frontsurf.oee.model.User;
import com.frontsurf.oee.util.Return;

/**
 * @author liu.zhiyang 2019/5/16 13:31
 * @Email liu.zhiyang@frontsurf.com
 */
public interface UserService {


    User getUserByAccountNum(String userName);

    /**
     * 用户列表
     * @return
     */
    Return userList();


    User getUserById(Integer id);

    Return userRole();
}
