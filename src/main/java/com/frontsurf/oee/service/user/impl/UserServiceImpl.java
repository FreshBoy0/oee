package com.frontsurf.oee.service.user.impl;

import com.frontsurf.oee.mapper.user.UserMapper;
import com.frontsurf.oee.model.Role;
import com.frontsurf.oee.model.User;
import com.frontsurf.oee.service.user.UserService;
import com.frontsurf.oee.util.Return;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author liu.zhiyang 2019/5/16 13:31
 * @Email liu.zhiyang@frontsurf.com
 */
@Service
public class UserServiceImpl implements UserService  {

    @Autowired
    UserMapper userMapper;

    @Override
    public User getUserByAccountNum(String userName) {
         return userMapper.selectByAccountNum(userName);
    }

    @Override
    public Return userList() {
        return Return.success(userMapper.userList()) ;
    }

    @Override
    public User getUserById(Integer id){ return userMapper.getUserById(id); }

    @Override
    public Return userRole() {
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        List<Role> roleList = user.getRoles();
        return Return.success(roleList);
    }
}
