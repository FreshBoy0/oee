package com.frontsurf.oee.service.deviceManage.impl;

import ch.qos.logback.core.net.SyslogOutputStream;
import com.frontsurf.oee.mapper.deviceManage.OeeDeviceLoadSchemeMapper;
import com.frontsurf.oee.model.OeeDeviceLoadScheme;
import com.frontsurf.oee.service.deviceManage.deviceLoadSchemeService;
import com.frontsurf.oee.util.Return;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.*;


@Service
public class deviceLoadSchemeServiceImpl implements deviceLoadSchemeService {

    @Autowired
    OeeDeviceLoadSchemeMapper oeeDeviceLoadSchemeMapper;


//    @Autowired
//    OeeDeviceMapper oeeDeviceMapper;
//
//    @Autowired
//    DeviceService deviceService;


    /**
     * 录入负荷计划信息
     *
     * @param OeeDeviceLoadScheme
     * @return
     */
    @Override
    public Return insertDeviceLoadScheme(@RequestBody OeeDeviceLoadScheme OeeDeviceLoadScheme)
    {
        String name = OeeDeviceLoadScheme.getName();
        OeeDeviceLoadScheme OeeDeviceLoadScheme1 = oeeDeviceLoadSchemeMapper.queryOeeDeviceLoadSchemeByName(name);
        if (OeeDeviceLoadScheme1 != null) {
            return Return.failForValidationErr("负荷计划名称已存在");
        }
        else {
            Integer tag = 0;
            try {
                tag = oeeDeviceLoadSchemeMapper.insert(OeeDeviceLoadScheme);
//                System.out.println(tag);
            }

            catch (Exception e) {
//                System.out.println(OeeDeviceLoadScheme);
                e.printStackTrace();
                return Return.fail("出现异常，设备负荷计划信息录入失败");
            }

            if (tag > 0) {
                return Return.success("负荷计划信息录入成功");
            }
            else
            {
                return Return.fail("负荷计划信息录入失败");
            }
        }
    }
    /**
     * 更新设备计划信息
     *
     * @param OeeDeviceLoadScheme
     * @return
     */
    @Override
    public Return updateDeviceLoadSchemeById(OeeDeviceLoadScheme OeeDeviceLoadScheme) {
        int id = OeeDeviceLoadScheme.getId();
//        System.out.println(id);
        OeeDeviceLoadScheme OeeDeviceLoadScheme1 = oeeDeviceLoadSchemeMapper.queryOeeDeviceLoadSchemeById(id);
        if (OeeDeviceLoadScheme1 == null) {
            return Return.fail("更新失败，该设备负荷计划不存在");
        }

        String name = OeeDeviceLoadScheme.getName();
        OeeDeviceLoadScheme OeeDeviceLoadScheme2 =null;
        if (name != null)
            OeeDeviceLoadScheme2 = oeeDeviceLoadSchemeMapper.queryOeeDeviceLoadSchemeByName(name);
        if (OeeDeviceLoadScheme2 != null) {
            int id1 = OeeDeviceLoadScheme2.getId();
            if (id != id1) {
                return Return.failForValidationErr("更新失败，工作计划名称已存在");
            }
        }

        int tag = 0;
        try {
            tag = oeeDeviceLoadSchemeMapper.updateOeeDeviceLoadSchemeById(OeeDeviceLoadScheme);
        } catch (Exception e) {
            e.printStackTrace();
            return Return.fail("出现异常，该设备负荷计划更新失败");
        }
        if (tag > 0) {
            return Return.success("设备负荷计划信息更新成功");
        } else {
            return Return.fail("设备的工作计划信息未更新");
        }

    }

    /**
     * 根据Id删除设备负荷计划信息
     *
     * @param id
     * @return
     */
    @Override
    public Return deleteDeviceLoadSchemeById(@RequestParam int id) {

            try {
                //删除设备负荷计划信息
                oeeDeviceLoadSchemeMapper.deleteDeviceLoadSchemeById(id);
            }
            catch (Exception e) {
                e.printStackTrace();
                return Return.fail("出现异常，设备工作计划信息删除失败");
            }
            return Return.success("删除成功");
        }


    /**
     * 根据负荷名称查询负荷计划信息
     *
     * @param name 负荷计划名称
     * @return
     */
    @Override
    public Return queryDeviceLoadSchemeByName(@RequestParam String name) {
        if (name == null) {
            return Return.failForValidationErr("设备计划名称不能为空");
        }
        OeeDeviceLoadScheme OeeDeviceLoadScheme = oeeDeviceLoadSchemeMapper.queryOeeDeviceLoadSchemeByName(name);
        if (OeeDeviceLoadScheme == null) {
            return Return.failForValidationErr("设备负荷计划: " + name + " 不存在，查询失败");
        } else return Return.success(OeeDeviceLoadScheme);
    }

    /**
     * 根据负荷id查询负荷计划信息
     *
     * @param
     * @return
     */
    @Override
    public Return queryOeeDeviceLoadSchemeById(Integer id) {
        if (id == null) {
            return Return.failForValidationErr("负荷计划Id不能为空");
        }
        OeeDeviceLoadScheme OeeDeviceLoadScheme = oeeDeviceLoadSchemeMapper.queryOeeDeviceLoadSchemeById(id);

        if (OeeDeviceLoadScheme == null) {
            return Return.failForValidationErr("设备负荷计划: " + id + " 不存在，查询失败");
            }

        else return Return.success(OeeDeviceLoadScheme);

    }

    /**
     * 查询所有的设备计划信息
     * 返回数据封装在list
     */
    @Override
    public Return queryAllDeviceLoadScheme() {
        try {
            List<OeeDeviceLoadScheme> OeeDeviceLoadSchemes = oeeDeviceLoadSchemeMapper.queryAllDeviceLoadScheme();
            if (OeeDeviceLoadSchemes != null)
                return Return.success(OeeDeviceLoadSchemes);
            else  return Return.failForValidationErr("设备负荷计划为空");
            }
        catch (Exception e) {
            return Return.fail("出现异常，查询失败");
        }
    }

}




