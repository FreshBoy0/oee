package com.frontsurf.oee.service.deviceManage.impl;

import ch.qos.logback.core.net.SyslogOutputStream;
import com.frontsurf.oee.mapper.deviceManage.BrandsTypes;
import com.frontsurf.oee.mapper.deviceManage.OeeDeviceBrandMapper;
import com.frontsurf.oee.mapper.deviceManage.OeeDeviceTypeMapper;
import com.frontsurf.oee.model.OeeBrands;
import com.frontsurf.oee.model.OeeBrandtypes;
import com.frontsurf.oee.model.OeeTypes;
import com.frontsurf.oee.service.deviceManage.BrandTypeService;
import com.frontsurf.oee.util.Return;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.List;

@Service
public class BrandTypeServiceImpl implements BrandTypeService {

    @Autowired
    OeeDeviceBrandMapper oeeDeviceBrandMapper;
    @Autowired
    OeeDeviceTypeMapper oeeDeviceTypeMapper;
    @Autowired
    BrandsTypes brandsTypes;


    /**
     * 录入品牌
     *
     * @param
     * @return
     */
    @Override
    public Return insertBrand(@RequestBody OeeBrandtypes oeeBrandtypes) {
        String brand_name = oeeBrandtypes.getBrand_name();
        System.out.println("品牌名:" + "brand_name");
        int OeeBrand1 = oeeDeviceBrandMapper.queryBrandByName(brand_name);
        System.out.println(OeeBrand1);

        if (OeeBrand1 > 0) {
            return Return.failForValidationErr("该品牌已经存在");
        }
        System.out.println(1);

        int tag =oeeDeviceBrandMapper.insert(brand_name);
        List<OeeTypes> OeeTypes = oeeBrandtypes.getTypesList();
        for (OeeTypes OeeType : OeeTypes) {
            String type_name = OeeType.getType_name();
            int OeeType1 = oeeDeviceTypeMapper.queryTypeByName(type_name);
            if (OeeType1 == 0) {
                try {
                    int tag1 =  oeeDeviceTypeMapper.insert(type_name);
                } catch (Exception e) {
                    e.printStackTrace();
                    return Return.fail("出现异常，录入型号失败");
                }
            }
        }
        System.out.println(2);
        int brandId = oeeDeviceBrandMapper.queryBrandIdByName(brand_name);
        for (OeeTypes OeeType2 : OeeTypes) {
            String type_name = OeeType2.getType_name();
            System.out.println("######");
            int typeId = oeeDeviceTypeMapper.queryTypeIdByName(type_name);
//            System.out.println(typeId.getType_id());
            System.out.println(type_name);
            System.out.println(brand_name);
            System.out.println(typeId);
            System.out.println(brandId);
//            int type_id = typeId.getType_id();
//            int brand_id = brandId.getBrand_id();
            try {
                int tag2 = brandsTypes.insert(brandId,typeId );
            } catch (Exception e) {
                e.printStackTrace();
                return Return.fail("出现异常，录入关联id失败");
            }

        }
        return Return.success("品牌型号信息录入成功");
    }






    /**
     * 根据品牌名查看对应的型号名
     *
     * @param
     * @return
     */
    @Override
    public Return queryTypeNameByBrandName(@RequestParam String brandName){
        ArrayList typeNameList = new ArrayList();
        int brandId = oeeDeviceBrandMapper.queryBrandIdByName(brandName);
        List<Integer> typesId = brandsTypes.selectTypesIdByBrandId(brandId);
        for (Integer typeId:typesId) {
            String typeName6 = oeeDeviceTypeMapper.queryTypeNameById(typeId);
            typeNameList.add(typeName6);
        }
        return  Return.success(typeNameList,"查看型号名成功");

    }

}













