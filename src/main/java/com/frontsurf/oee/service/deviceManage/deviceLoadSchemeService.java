package com.frontsurf.oee.service.deviceManage;

import com.frontsurf.oee.model.OeeDeviceLoadScheme;
import com.frontsurf.oee.util.Return;

import java.util.List;

public interface deviceLoadSchemeService {

    /**
     * 录入设备负荷计划信息
     *
     * @param
     * @return
     */
    Return insertDeviceLoadScheme(OeeDeviceLoadScheme oeeDeviceLoadScheme);

    /**
     * 更新设备负荷计划信息
     * @param
     * @return
     */
    Return updateDeviceLoadSchemeById(OeeDeviceLoadScheme oeeDeviceLoadScheme);

    /**
     * 根据Id删除设备负荷计划信息
     *
     * @param id
     * @return
     */
    Return deleteDeviceLoadSchemeById(int id);

    /**
     * 根据负荷计划名称查询设备负荷计划信息
     * @param name 模式名称
     * @return
     */
    Return queryDeviceLoadSchemeByName(String name);
    /**
     * 根据id查询负荷计划信息
     *
     */
    Return queryOeeDeviceLoadSchemeById( Integer id);

    /**
     * 查询所有的设备负荷计划信息
     * @return
     */
    Return queryAllDeviceLoadScheme();










}
