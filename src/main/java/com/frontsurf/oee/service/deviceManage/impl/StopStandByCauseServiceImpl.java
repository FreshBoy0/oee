package com.frontsurf.oee.service.deviceManage.impl;

import com.frontsurf.oee.mapper.deviceManage.OeeDeviceTypeMapper;
import com.frontsurf.oee.mapper.deviceManage.OeeStopStandByReasonTypesMapper;
import com.frontsurf.oee.mapper.deviceManage.OeeStopStandByReasonsMapper;
import com.frontsurf.oee.model.OeeDevice;
import com.frontsurf.oee.model.OeeStopStandbyReasons;
import com.frontsurf.oee.model.OeeTypes;
import com.frontsurf.oee.service.deviceManage.StopStandByCauseService;
import com.frontsurf.oee.util.Return;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.ArrayList;
import java.util.List;



@Service
public class StopStandByCauseServiceImpl implements StopStandByCauseService {

@Autowired
OeeStopStandByReasonsMapper  oeeStopStandByReasonsMapper;

@Autowired
OeeStopStandByReasonTypesMapper     oeeStopStandByReasonTypesMapper;

@Autowired
OeeDeviceTypeMapper oeeDeviceTypeMapper;


    @Override
    public Return insertStopStandByCause(@RequestBody OeeStopStandbyReasons oeeStopStandbyReasons){
        //获取下属型号
        List<OeeTypes> oeeTypeList = oeeStopStandbyReasons.getOeeTypes();
        System.out.println(oeeTypeList);
        oeeStopStandByReasonsMapper.insert(oeeStopStandbyReasons);
        Integer id = oeeStopStandbyReasons.getId();
        System.out.println(id);
        List<Integer> typeIDs = new ArrayList<>();

        for (OeeTypes oeeType:oeeTypeList)
        {
//            if (oeeType == null) return Return.fail(Return.VALIDATION_ERROR, "型号参数有误!");
            Integer typeID = oeeDeviceTypeMapper.queryTypeIdByName(oeeType.getType_name());
            System.out.println(typeID);
            if (typeID == 0) return Return.fail(Return.VALIDATION_ERROR, "所选型号不存在!");
            typeIDs.add(typeID);
        }
        System.out.println(typeIDs);
        for (Integer typeID :typeIDs)
        {
            oeeStopStandByReasonTypesMapper.insert(id, typeID);
        }

        return Return.success("新增停待机原因成功！");



    }























}
