package com.frontsurf.oee.service.deviceManage;

import com.frontsurf.oee.model.OeeBrandtypes;
import com.frontsurf.oee.model.OeeDevice;
import com.frontsurf.oee.util.Return;

public interface BrandTypeService {


//    Return insertBrandTypes(OeeBrandtypes oeeBrandtypes);

    /**
     * 根据品牌名查询型号名称
     * @param brandName
     * @return
     */
    Return queryTypeNameByBrandName(String brandName);

    /**
     *插入品牌名称
     * @param oeeBrandtypes
     * @return
     */
    Return insertBrand(OeeBrandtypes oeeBrandtypes);









}
