package com.frontsurf.oee.service.deviceManage.impl;

import com.frontsurf.oee.mapper.deviceManage.OeeDeviceLoadSchemeMapper;
import com.frontsurf.oee.mapper.deviceManage.OeeDeviceMapper;
import com.frontsurf.oee.model.OeeDevice;
import com.frontsurf.oee.service.deviceManage.DeviceService;
import com.frontsurf.oee.util.Return;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import io.netty.util.internal.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.LinkedList;
import java.util.List;
import java.util.Set;

@Service
public class DeviceServiceImpl implements DeviceService {

    @Autowired
    OeeDeviceMapper oeeDeviceMapper;
    
    @Autowired
    OeeDeviceLoadSchemeMapper oeeDeviceAttendanceSchemaMapper;

//    @Autowired

//    private OeeDeviceTypeMapper oeeDeviceTypeMapper;
    @Override
    public Return insertDevice(@RequestBody OeeDevice oeeDevice) {
        String serialNum = oeeDevice.getSerialNum();
        OeeDevice oeeDevice1 = oeeDeviceMapper.queryDeviceBySerialNum(serialNum);
        if (oeeDevice1 != null) {
            return Return.failForValidationErr("设备编号已存在");
        }
//        String name = oeeDevice.getName();
//        if (name != null) {
//            return Return.failForValidationErr("设备名称已存在");
//            }
//        }

        int tag = 0;
        try {
//            System.out.println("----1----" + oeeDevice.getDeviceMode());
            tag = oeeDeviceMapper.insert(oeeDevice);
        } catch (Exception e) {
            e.printStackTrace();
            return Return.fail("出现异常，设备数据录入失败");
        }
        if (tag > 0) {
            try {
                Integer diffTime = oeeDevice.getStandByTime();
                Integer offTime = oeeDevice.getOffLineTime();
                if (diffTime == null || offTime == null) {
                    return Return.failForValidationErr("转待机或转离线不能为空");
                }
            }
            catch (Exception e) {
                Return.fail("设备录入成功，但是转待机、转离线录入失败");
            }
    
            return Return.success("设备数据录入成功");
        } else {
            return Return.fail("设备数据未录入");
        }

}

    /**
     * 更新设备信息
     *
     * @param oeeDevice
     * @return
     */
    @Override
    public Return updateDeviceById(@RequestBody OeeDevice oeeDevice) {
        Integer id = oeeDevice.getId();
        OeeDevice oeeDevice1 = oeeDeviceMapper.queryDeviceById(id);
        if (oeeDevice1 == null) {
            return Return.failForValidationErr("该设备不存在,数据更新失败");
        }

//        String serialNum = oeeDevice.getSerialNum();
//        if (serialNum != null) {
//            OeeDevice oeeDevice2 = oeeDeviceMapper.queryDeviceBySerialNum(serialNum);
//            if (oeeDevice2 != null) {
//                if (!id.equals(oeeDevice2.getId())) {
//                    return Return.failForValidationErr("设备编号已存在,数据更新失败");
//                }
//            }
//        }

        int tag = 0;
        try {
            tag = oeeDeviceMapper.updateDeviceById(oeeDevice);
        } catch (Exception e) {
            e.printStackTrace();
            return Return.fail("出现异常，设备信息更新失败");
        }
        if (tag > 0) {
            return Return.success("设备信息更新成功");
        } else {
            return Return.fail("设备信息未更新");
        }
    }

    /**
     * 根据id删除设备
     *
     * @param id
     * @return
     */
    @Override
    @Transactional
    public Return deleteDeviceById(@RequestParam  int id) {
        OeeDevice oeeDevice = oeeDeviceMapper.queryDeviceById(id);
        if (oeeDevice == null) {
            return Return.fail("设备档案Id不存在");
        }
        //标记删除成功的设备信息
        try {
//            for (Integer id : ids) {
//                //删除redis的数据
//                OeeDevice oeeDevice = oeeDeviceMapper.queryDeviceById(id);
//                String userOeeID = UserInfo.getUserOeeID();
//                String serialNum = oeeDevice.getSerialNum();
//                System.out.println("delete-redis-key::: " + "DEVICESTATUS:" + userOeeID + "|" + serialNum);
//                redisTemplate.delete("DEVICESTATUS:" + userOeeID + "|" + serialNum);
//                //删除与模具的关联关系
//                oeeDeviceMapper.deleteMoldDeviceByDeviceId(id);
                //删除设备
                oeeDeviceMapper.deleteDeviceById(id);
//               tags.add(id);
//            }
//            if (tags.size() == 0) {
//                return Return.fail("设备档案id不能为空");
//            } else {
                return Return.success("设备信息删除成功");
            }
         catch (Exception e) {
            e.printStackTrace();
            return Return.fail("删除设备失败");
        }
    }

    /**
     * 查询所有的设备档案
     *
     * @return
     */
    @Override
    public Return queryAllDevice() {
        try {
            List<OeeDevice> oeeDevices = oeeDeviceMapper.queryAllDevice();
            return Return.success(oeeDevices);
        } catch (Exception e) {
            e.printStackTrace();
            return Return.fail("查询设备档案失败");
        }
    }

    /**
     * 根据设备编号查询设备类型信息
     *
     * @param serialNum
     * @return
     */
    @Override
    public Return queryDeviceBySerialNum(@RequestParam String serialNum) {
        if (StringUtil.isNullOrEmpty(serialNum)) {
            return Return.failForValidationErr("设备编号不能为空2");
        }
        try {
            OeeDevice OeeDevice = oeeDeviceMapper.queryDeviceBySerialNum(serialNum);
            return Return.success(OeeDevice);
        } catch (Exception e) {
            e.printStackTrace();
            return Return.fail("查询设备档案失败");
        }
    }


    /**
     * 根据设备名查询设备信息
     *
     * @param name
     * @return
     */
    @Override
    public Return queryDeviceByName(@RequestParam String name) {
        if (StringUtil.isNullOrEmpty(name)) {
            return Return.failForValidationErr("设备名称不能为空");
        }
        try {
            List<OeeDevice> OeeDevices = oeeDeviceMapper.queryDeviceByName(name);
            return Return.success(OeeDevices);
        } catch (Exception e) {
            e.printStackTrace();
            return Return.fail("查询设备档案失败");
        }
    }












}
