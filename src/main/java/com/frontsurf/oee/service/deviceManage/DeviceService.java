package com.frontsurf.oee.service.deviceManage;

import com.frontsurf.oee.model.OeeDevice;
import com.frontsurf.oee.util.Return;

import java.util.List;
import java.util.Set;

public interface DeviceService {

    /**
     * 录入设备信息
     *
     * @param oeeDevice
     * @return
     */
    Return insertDevice(OeeDevice oeeDevice);

    /**
     * 根据id更新设备信息
     *
     * @param oeeDevice
     * @return
     */
    Return updateDeviceById(OeeDevice oeeDevice);

    /**
     * 根据Id删除设备信息
     *
     * @param id
     * @return
     */
    Return deleteDeviceById(int id);
    /**
     * 根据设备编号查询设备信息
     * @return
     */
    Return queryDeviceBySerialNum(String SerialNum);
    /**
     * 根据设备id查询设备信息
     * @return
     */
//    Return queryDeviceById(int id);
    /**
     * 根据设备名称查询设备信息
     * @return
     */
    Return queryDeviceByName(String name);
    /**
     * 查询所有的设备信息
     * @return
     */
    Return queryAllDevice();





















}
