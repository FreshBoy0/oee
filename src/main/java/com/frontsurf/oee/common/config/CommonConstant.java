package com.frontsurf.oee.common.config;


/**
 * @Author xu.xiaojing
 * @Date 2019/6/19 14:55
 * @Email xu.xiaojing@frontsurf.com
 * @Description 上下文的常量
 * 建议将一些状态常量、类型常量在此处进行管理
 */
public interface CommonConstant {

    /**
     * 生产稳定性评估级别的设置
     */
    String PRODUCTION_THRESHOLD_EXCELLENT = "production_threshold_excellent";
    String PRODUCTION_THRESHOLD_GOOD_MAX = "production_threshold_good_max";
    String PRODUCTION_THRESHOLD_GOOD_MIN = "production_threshold_good_min";
    String PRODUCTION_THRESHOLD_MIDDLE_MAX = "production_threshold_middle_max";
    String PRODUCTION_THRESHOLD_MIDDLE_MIN = "production_threshold_middle_min";
    String PRODUCTION_THRESHOLD_QUALIFIED_MAX = "production_threshold_qualified_max";
    String PRODUCTION_THRESHOLD_QUALIFIED_MIN = "production_threshold_qualified_min";

    /**
     * 工艺卡有效性阈值设置
     */
    String CRAFTCARD_THRESHOLD_EXCELLENT = "craftcard_threshold_excellent";
    String CRAFTCARD_THRESHOLD_GOOD_MAX = "craftcard_threshold_good_max";
    String CRAFTCARD_THRESHOLD_GOOD_MIN = "craftcard_threshold_good_min";
    String CRAFTCARD_THRESHOLD_MIDDLE_MAX = "craftcard_threshold_middle_max";
    String CRAFTCARD_THRESHOLD_MIDDLE_MIN = "craftcard_threshold_middle_min";
    String CRAFTCARD_THRESHOLD_QUALIFIED_MAX = "craftcard_threshold_qualified_max";
    String CRAFTCARD_THRESHOLD_QUALIFIED_MIN = "craftcard_threshold_qualified_min";

    /**
     * 参数类型
     */
    String VALUE_BOOLEAN = "Boolean";
    String VALUE_INTEGER = "Integer";
    String VALUE_DOUBLE = "Double";
    String VALUE_STRING = "String";

    /**
     * 缓存设置
     */
    String CACHE_NAME_DAYCACHE = "dayCache";
    String CACHE_NAME_BASECACHE = "basicCache";
    String CACHE_KEY_GENERATOR_WISE = "wiselyKeyGenerator";

}
