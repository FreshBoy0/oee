//package com.frontsurf.deviceOee.common.config;
//
//import com.frontsurf.deviceOee.service.craftRecord.AlarmReceiver;
//import com.frontsurf.deviceOee.service.craftRecord.CraftRecordReceiver;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.data.redis.connection.RedisConnectionFactory;
//import org.springframework.data.redis.listener.PatternTopic;
//import org.springframework.data.redis.listener.RedisMessageListenerContainer;
//import org.springframework.data.redis.listener.adapter.MessageListenerAdapter;
//
///**
// * 订阅者配置
// *
// * @author liu.zhiyang 2019/5/21 11:36
// * @Email liu.zhiyang@frontsurf.com
// */
//@Configuration
//public class ReceiverConfig {
//    @Value("${alarmTopic}")
//    private String alarmTopic;
//    @Value("${recordTopic}")
//    private String recordTopic;
//
//    @Bean
//    public RedisMessageListenerContainer container(RedisConnectionFactory connectionFactory, MessageListenerAdapter craftRecordListenerAdapter,
//                                                   MessageListenerAdapter alarmListenerAdapter) {
//        RedisMessageListenerContainer container = new RedisMessageListenerContainer();
//        container.setConnectionFactory(connectionFactory);
//        container.addMessageListener(craftRecordListenerAdapter, new PatternTopic(recordTopic));
//        container.addMessageListener(alarmListenerAdapter, new PatternTopic(alarmTopic));
//        return container;
//    }
//
//    @Bean
//    public MessageListenerAdapter craftRecordListenerAdapter(CraftRecordReceiver receiver) {
//        return new MessageListenerAdapter(receiver, "receiveMessage");
//    }
//
//    @Bean
//    public MessageListenerAdapter alarmListenerAdapter(AlarmReceiver alarmReceiver) {
//        return new MessageListenerAdapter(alarmReceiver, "receiveMessage");
//    }
//
//
//    @Bean
//    public CraftRecordReceiver craftRecordReceiver() {
//        return new CraftRecordReceiver();
//    }
//
//    @Bean
//    public AlarmReceiver alarmReceiver() {
//        return new AlarmReceiver();
//    }
//
//}
