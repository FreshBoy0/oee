package com.frontsurf.oee.common.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

/**
 * @author liu.zhiyang 2018/12/13 9:34
 * @Email liu.zhiyang@frontsurf.com
 */
@Configuration
public class GlobalConfig {

    public final static String SESSION_KEY="userName";

    /**
     * redis过期时间
     */
    @Value("${redisExpireTime}")
    public  int redisExpireTime;





    public int getRedisExpireTime() {
        return redisExpireTime;
    }

    public void setRedisExpireTime(int redisExpireTime) {
        this.redisExpireTime = redisExpireTime;
    }
}
