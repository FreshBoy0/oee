package com.frontsurf.oee.common.config;

/**
 * 消息订阅者
 * @author liu.zhiyang 2019/5/21 11:42
 * @Email liu.zhiyang@frontsurf.com
 */
public abstract  class AbstractReceiver {
    public abstract void receiveMessage(Object message);
}
