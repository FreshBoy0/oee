//package com.frontsurf.cma.common.config;
//
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.web.socket.server.standard.ServerEndpointExporter;
//
///**
// * 部署到linux上时，要注释掉这个类
// * @author liu.zhiyang 2019/5/21 13:49
// * @Email liu.zhiyang@frontsurf.com
// */
//@Configuration
//public class WebSocketConfig {
//    @Bean
//    public ServerEndpointExporter serverEndpointExporter() {
//        return new ServerEndpointExporter();
//    }
//}
