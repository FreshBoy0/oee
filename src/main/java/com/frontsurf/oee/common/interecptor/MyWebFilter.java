package com.frontsurf.oee.common.interecptor;

import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @Author xu.xiaojing
 * @Date 2019/3/27 16:24
 * @Email xu.xiaojing@frontsurf.com
 * @Description handle 跨域问题
 */

@Component
@WebFilter(urlPatterns = "/*")
public class MyWebFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {

        HttpServletResponse response = (HttpServletResponse) servletResponse;
        response.setHeader("Access-Control-Allow-Origin","*");
        response.addHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE");
        //30 min
        response.addHeader("Access-Control-Max-Age", "1800");
        filterChain.doFilter(servletRequest,servletResponse);
     //   System.out.println(response.getStatus());
    }

    @Override
    public void destroy() {

    }
}
