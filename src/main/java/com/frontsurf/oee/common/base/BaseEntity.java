package com.frontsurf.oee.common.base;


import com.frontsurf.oee.common.group.Update;
import org.springframework.data.annotation.Id;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @Author xu.xiaojing
 * @Date 2018/9/16 10:31
 * @Email xu.xiaojing@frontsurf.com
 * @Description  最基础的类，负责：序列化、ID的生成策略
 */


@MappedSuperclass
public abstract class BaseEntity<ID extends Serializable> implements Serializable {
    private static final long serialVersionUID = 1L;

    @NotNull(message = "id不能为空",groups = {Update.class})
    private ID id;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public ID getId() {
        return id;
    }

    public void setId(ID id) {
        this.id = id;
    }

}
