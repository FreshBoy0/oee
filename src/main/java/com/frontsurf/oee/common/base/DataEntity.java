package com.frontsurf.oee.common.base;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.MappedSuperclass;
import java.io.Serializable;
import java.util.Date;

/**
 * @Author xu.xiaojing
 * @Date 2018/9/16 10:34
 * @Email xu.xiaojing@frontsurf.com
 * @Description
 */

@MappedSuperclass
public abstract class DataEntity<ID extends Serializable> extends BaseEntity<ID> {


    protected Integer createBy;    // 创建者
    protected String createName;   // 创建者名称
    protected Date createDate;    // 创建日期
    protected Integer updateBy;    // 更新者
    protected String updateName;   // 更新者名称
    protected Date updateDate;    // 更新日期
//    protected Integer delFlag = 0;    // 删除标记（0：正常；1：删除）

    public static final String DEL_FLAG_NORMAL = "0";
    public static final String DEL_FLAG_DELETE = "1";

    public Integer getCreateBy() {
        return createBy;
    }

    public void setCreateBy(Integer createBy) {
        this.createBy = createBy;
    }

    @JsonFormat(pattern = "yyyy-MM-dd")
    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Integer getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(Integer updateBy) {
        this.updateBy = updateBy;
    }

//    @JsonFormat(pattern = "yyyy-MM-dd")
    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getCreateName() {
        return createName;
    }

    public void setCreateName(String createName) {
        this.createName = createName;
    }

    public String getUpdateName() {
        return updateName;
    }

    public void setUpdateName(String updateName) {
        this.updateName = updateName;
    }

}
