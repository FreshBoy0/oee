package com.frontsurf.oee;

import com.github.pagehelper.PageHelper;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;

import javax.annotation.PostConstruct;
import java.util.Properties;
import java.util.TimeZone;

/**
 * @author liu.zhiyang 2019/5/16 10:14
 * @Email liu.zhiyang@frontsurf.com
 */
@SpringBootApplication
@MapperScan({"com.frontsurf.oee.mapper"})
@EnableCaching
public class MainConfig extends SpringBootServletInitializer {
    //  private static Logger logger = LoggerFactory.getLogger(MainConfig.class);

    public static void main(String[] args) {
        SpringApplication springApplication = new SpringApplication(MainConfig.class);
        ConfigurableApplicationContext run = springApplication.run(args);
//        logger.info("启动成功");
    }


    @PostConstruct
    void started() {
        TimeZone.setDefault(TimeZone.getTimeZone("GMT+8"));
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(MainConfig.class);
    }

    /**
     * 配置mybatis的分页插件pageHelper
     */
    @Bean
    public PageHelper pageHelper() {
        PageHelper pageHelper = new PageHelper();
        Properties properties = new Properties();
        properties.setProperty("offsetAsPageNum", "true");
        properties.setProperty("rowBoundsWithCount", "true");
        properties.setProperty("reasonable", "true");
        //配置mysql数据库的方言
        properties.setProperty("dialect", "mysql");
        pageHelper.setProperties(properties);
        return pageHelper;
    }
}
