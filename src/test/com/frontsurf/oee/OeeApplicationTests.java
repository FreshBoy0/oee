package com.frontsurf.oee;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class OeeApplicationTests {

    @Test
    public void contextLoads() {
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        String hashedPassword = passwordEncoder.encode("123456");
        System.out.println(hashedPassword);

        boolean pswFlag = passwordEncoder.matches("password","$2a$10$alT1yTW/dEmBwxz1FiJzje/cjpV2lqfvECfVdLtDGZH9tbhnEhofy");//解密

        System.out.println(pswFlag);
    }

}
