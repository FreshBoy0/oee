package com.frontsurf.oee;


import com.alibaba.fastjson.JSON;
import com.frontsurf.oee.controller.deviceManage.DeviceController;
import com.frontsurf.oee.model.OeeDevices;
import com.frontsurf.oee.util.Return;
import com.google.gson.Gson;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.client.MockMvcClientHttpRequestFactory;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import java.util.Date;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest  //查找@SpringBootApplication，使用它启动应用
@AutoConfigureMockMvc // 注入MockMvc mvc
public class DeviceListControllerTest {
    @Autowired
    private DeviceController DeviceController;
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private WebApplicationContext webApplicationContext;

    Gson gson = new Gson();

      String addDevice = "/device/addDevice";
      String queryDeviceBySerialNum = "/device/queryDeviceBySerialNum";
      String updateDeviceById = "/device/updateDeviceById";
      String queryDeviceByName = "/device/queryDeviceByName";


String body = "{\n" +
        "\t\"id\":3,\n" +
        "\t\"name\":\"设备七\",\n" +
        "\t\"serialNum\":\"qwe4563\",\n" +
        "\t\"deviceType\":\"180T\",\n" +
        "\t\"dieChangeTime\":20,\n" +
        "\t\"offLineTime\":18,\n" +
        "\t\"standByTime\":38,\n" +
        "\t\"loadPlan\":\"计划三\",\n" +
        "\t\"deviceBrand\":\"泰瑞0\",\n" +
        "\t\"delFlag\":0\n" +
        "}";



    @Before    //实例化MockMvc
    public void setUp() throws Exception{
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();//建议使用这种
    }

    @Test
    public void queryDeviceBySerialNum() throws Exception {
        assertThat(DeviceController).isNotNull(); //程序assertion
        mockMvc.perform(MockMvcRequestBuilders.get(queryDeviceBySerialNum)
                .param("SerialNum" , "1234")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print());
    }

    @Test
    public void queryDeviceByName() throws Exception{
        mockMvc.perform(MockMvcRequestBuilders.get(queryDeviceByName)
                .param("name","设备五")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print());
    }

    @Test
    public void addDevice() throws Exception {
       // OeeDevices userModel = new OeeDevices();
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.post(addDevice)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content( body )  //请求体 String
                //.content(JSON.toJSONString(userModel))
                .accept(MediaType.APPLICATION_JSON)) // 断言返回结果是json
                .andReturn();// 得到返回结果
        MockHttpServletResponse response = mvcResult.getResponse();
        int status = response.getStatus(); //拿到请求返回码
        String contentAsString = response.getContentAsString(); //拿到结果
        Assert.assertEquals(200,status);//System.err.println(status);
        System.err.println(contentAsString);
    }

    @Test
    public void updateDeviceById() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post(updateDeviceById)
                .content(body)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print());
    }




}
